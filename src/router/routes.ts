import { RouteRecordRaw } from 'vue-router';

export enum Pages {
  home = 'home',
  login = 'login',
  groupUsers = 'nhom-nguoi-dung',
  cmsUser = 'nguoi-dung',
  managingUnit = 'don-vi-chu-quan',
  artist = 'nghe-sy',

  informationArtist = 'cap-nhat-thong-tin-nghe-sy',
  customer = 'khach-hang',
  addArtist = 'them-nghe-sy',
  customerRank = 'xep-hang-khach-hang',
  artistRank = 'Xep-hang-nghi-sy',
  field = 'linh-vuc-hoat-dong',
  post = 'bai-viet',
  postCategory = 'danh-muc-bai-viet',
  banner = 'banner',
  infoVAB = 'thong-tin-chung',
  menu = 'menu',
  listBooking = 'danh-sach-booking',
  work = 'cong-viec',
  configSystem = 'cau-hinh-trang-tinh',
  news = 'cau-hinh-tin-tuc',
  hotProduct = 'cau-hinh-san-pham-noi-bat',
  configPartner = 'cau-hinh-doi-tac-truyen-thong',
}

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/Index.vue'),
        name: Pages.home,
      },
      {
        path: '/login',
        component: () => import('pages/login/index.vue'),
        name: Pages.login,
      },
      {
        path: '/nhom-nguoi-dung',
        component: () => import('pages/nhom-nguoi-dung/index.vue'),
        name: Pages.groupUsers,
      },
      {
        path: '/nguoi-dung',
        component: () => import('pages/nguoi-dung/index.vue'),
        name: Pages.cmsUser,
      },
      {
        path: '/don-vi-chu-quan',
        component: () => import('pages/don-vi-chu-quan/index.vue'),
        name: Pages.managingUnit,
      },
      {
        path: '/nghe-sy',
        component: () => import('pages/nghe-sy/index.vue'),
        name: Pages.artist,
      },
      {
        path: '/nghe-sy/cap-nhat-thong-tin-nghe-sy/:id',
        component: () => import('pages/cap-nhat-thong-tin-nghe-sy/index.vue'),
        name: Pages.informationArtist,
      },
      {
        path: '/nghe-sy/them-nghe-sy',
        component: () => import('pages/them-nghe-sy/index.vue'),
        name: Pages.addArtist,
      },
      {
        path: '/xep-hang-nghe-sy',
        component: () => import('pages/xep-hang-nghe-sy/index.vue'),
        name:Pages.artistRank
      },
      {
        path: '/khach-hang',
        component: () => import('pages/khach-hang/index.vue'),
        name: Pages.customer,
      },
      {
        path: 'xep-hang-khach-hang',
        component: () => import('pages/xep-hang-khach-hang/index.vue'),
        name: Pages.customerRank,
      },
      {
        path: 'linh-vuc-hoat-dong',
        component: () => import('pages/linh-vuc-hoat-dong/index.vue'),
        name: Pages.field,
      },
      {
        path: 'bai-viet',
        component: () => import('pages/bai-viet/index.vue'),
        name: Pages.post,
      },
      {
        path: 'danh-muc-bai-viet',
        component: () => import('pages/danh-muc-bai-viet/index.vue'),
        name: Pages.postCategory,
      },
      {
        path: 'banner',
        component: () => import('pages/banner/index.vue'),
        name: Pages.banner,
      },
      {
        path: 'thong-tin-chung',
        component: () => import('pages/thong-tin-chung/index.vue'),
        name: Pages.infoVAB,
      },
      {
        path: 'menu',
        component: () => import('pages/menu/index.vue'),
        name: Pages.menu,
      },
      {
        path: 'danh-sach-booking',
        component: () => import('pages/danh-sach-booking/index.vue'),
        name: Pages.listBooking,
      },
      {
        path: 'cong-viec',
        component: () => import('pages/cong-viec/index.vue'),
        name: Pages.work,
      },
      {
        path: 'cau-hinh-trang-tinh',
        component: () => import('pages/cau-hinh-trang-tinh/index.vue'),
        name: Pages.configSystem,
      },
      {
        path: 'cau-hinh-tin-tuc',
        component: () => import('pages/cau-hinh-tin-tuc/index.vue'),
        name: Pages.news,
      },
      {
        path: 'cau-hinh-san-pham-noi-bat',
        component: () => import('pages/cau-hinh-san-pham-noi-bat/index.vue'),
        name: Pages.hotProduct,
      },
      {
        path: 'cau-hinh-doi-tac-truyen-thong',
        component: () =>
          import('pages/cau-hinh-doi-tac-truyen-thong/index.vue'),
        name: Pages.configPartner,
      },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue'),
  },
];

export default routes;
