import { StateInterface } from './../../store/index';
import { defineComponent, ref } from 'vue';
import { Router, useRouter } from 'vue-router';
import { useStore } from 'src/store';
import { i18n } from 'src/boot/i18n';
import { Store } from 'vuex';
import { Pages } from 'src/router/routes';

const user_name = ref('');
const password = ref('');
const loggingIn = ref(false);

const login = async (router: Router, store: Store<StateInterface>) => {
  try {
    await store.dispatch('authentication/callAPILogin', {
      userName: user_name.value,
      password: password.value,
    });
  } catch (error) {}
};

export const Login = defineComponent({
  name: Pages.login,
  setup() {
    const router = useRouter();
    const store = useStore();
    const usernameInputRules = [
      (val: string) =>
        (val && val.trim().length > 0) ||
        i18n.global.t('loginPage.requireUsername'),

        (val: string) => 
        (val && /^[a-zA-Z0-9\+]*$/.test(val)) || 'Tên đăng nhập không hợp lệ'
      
    ];
    // console.log(isOK)

    const passwordInputRules = [
      
      (val: string) =>
        (val && val.trim().length > 0) ||
        i18n.global.t('loginPage.requirePassword'),
        
       
     
    ];

  

    const resetForm = () => {
      user_name.value = '';
      password.value = '';
    };

    const onSubmit = async () => {
      if (!loggingIn.value) {
        loggingIn.value = true;
        await login(router, store);
        loggingIn.value = false;
        // ...
      }
    };

    return {
      isPwd: ref(true),
      usernameInputRules,
      passwordInputRules,
      user_name,
      password,
      resetForm,
      onSubmit,
      loggingIn,
    };
  },
});
