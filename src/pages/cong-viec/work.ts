import { i18n } from 'src/boot/i18n';
import { defineComponent, onMounted, Ref, ref } from 'vue';
import Pagination from 'components/pagination/index.vue';
import { BookingStatus } from 'src/assets/enums';
import { ActiveStatus } from 'src/assets/enums';
export default defineComponent({
  components: {
    Pagination,
  },
  setup() {
    const tableColumns = [
      {
        name: 'stt',
        field: 'stt',
        required: true,
        label: i18n.global.t('work.titleColumnsTable.stt'),
        align: 'center',
        sortable: false,
      },
      {
        name: 'workName',
        field: 'workName',
        required: true,
        label: i18n.global.t('work.titleColumnsTable.workName'),
        align: 'center',
        headerStyle: 'text-align: center !important;',
        sortable: false,
      },
      {
        name: 'fieldName',
        field: 'fieldName',
        required: true,
        label: i18n.global.t('work.titleColumnsTable.fieldName'),
        align: 'center',
        headerStyle: 'text-align: center !important;',
        sortable: false,
      },
      {
        name: 'description',
        field: 'description',
        required: true,
        label: i18n.global.t('work.titleColumnsTable.description'),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'createBy',
        field: 'createBy',
        required: true,
        label: i18n.global.t('work.titleColumnsTable.createBy'),
        headerStyle: 'text-align: center !important;',
        align: 'center',
        sortable: false,
      },
      {
        name: 'createTime',
        field: 'createTime',
        required: true,
        label: i18n.global.t('work.titleColumnsTable.createTime'),
        headerStyle: 'text-align: center !important;',
        align: 'center',
        sortable: false,
      },
      {
        name: 'updateBy',
        field: 'updateBy',
        required: true,
        label: i18n.global.t('work.titleColumnsTable.updateBy'),
        headerStyle: 'text-align: center !important;',
        align: 'center',
        sortable: false,
      },
      {
        name: 'updateTime',
        field: 'updateTime',
        required: true,
        label: i18n.global.t('work.titleColumnsTable.updateTime'),
        headerStyle: 'text-align: center !important;',
        align: 'center',
        sortable: false,
      },
      {
        name: 'action',
        field: 'action',
        required: true,
        label: i18n.global.t('work.titleColumnsTable.action'),
        align: 'center',
        sortable: false,
      },
    ];
    const listWorks = ref([
      {
        stt: 1,
        workName: 'Ca sỹ',
        fieldName: 'Âm nhạc',
        description: 'Mô tả',
        createBy: 'admin',
        createTime: '18:00:00 - 30/4/2021',
        updateBy: 'nhanvien',
        updateTime: '20:00:00 - 30/4/2021',
        status: 1,
      },
    ]);
    const pageIndex = ref(1);
    const pageSize = ref(20);
    const totalPage = ref(10);
    const keywordSearch: Ref<string | null> = ref(null);
    const changePageSize = () => {
      pageIndex.value = 1;
      void getListWorks();
    };

    const getListWorks = () => {
      // console.log('API List Menu');
    };

    onMounted(() => {
      void getListWorks();
    });
    return {
      keywordSearch,
      status,
      listWorks,
      tableColumns,
      pageIndex,
      pageSize,
      totalPage,
      changePageSize,
      getListWorks,
      BookingStatus,
      ActiveStatus,
    };
  },
});
