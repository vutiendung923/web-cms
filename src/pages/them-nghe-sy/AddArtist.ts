import { defineComponent, onMounted, ref, Ref, watch } from 'vue';
import { api, BaseResponseBody } from 'src/boot/axios';
import { API_PATHS, config } from 'src/assets/configurations.example';
import { useRoute } from 'vue-router';
import { AxiosResponse } from 'axios';
import { i18n } from 'src/boot/i18n';
import { Dialog, Notify } from 'quasar';
import { Router } from 'src/router';
import { Pages } from 'src/router/routes';
import moment from 'moment';
import PersonalInformation from 'components/artist-information/personal-information/index.vue';
import VabAccount from 'components/artist-information/VAB-account/index.vue';
import BankAccount from 'components/artist-information/bank-account/index.vue';
import HotProduct from 'components/artist-information/hot-product/index.vue';
import AddHotProductDialog from 'components/artist-information/AddHotProduct/index.vue';
import AddNewBankAccountDialog from 'components/artist-information/bank-account/add-new-bank-account-dialog/index.vue';
import EditBankAccountDialog from 'components/artist-information/bank-account/edit-bank-account/index.vue';
import UploadEmbedDialog from 'components/artist-information/upload-embed-dialog/index.vue';
import UpdateHotProduct from 'components/artist-information/UpdateHotProduct/index.vue';
import AddStory from 'components/artist-information/VAB-account/add-story/index.vue';
import UpdateStory from 'components/artist-information/VAB-account/update-story/index.vue';

import {
  FieldType,
  NationalityType,
  ArtistLevelType,
  QualificationType,
  WorkType,
  MusicType,
  BankAccountType,
  ProductType,
  BannerType,
  StoriesType,
  SchedulesType,
  CardBankType,
  TypeCardType,
  FileUploadType,
  ProvinceType,
} from 'src/assets/type';

export type AvatarType = {
  file?: File;
  url?: string | null;
};
export type changeIsDefault = {
  idxAcc: number;
  isDefault: number;
};
export default defineComponent({
  components: {
    PersonalInformation,
    VabAccount,
    BankAccount,
    HotProduct,
    AddNewBankAccountDialog,
    EditBankAccountDialog,
    AddHotProductDialog,
    UpdateHotProduct,
    UploadEmbedDialog,
    AddStory,
    UpdateStory,
  },

  setup() {
    const route = useRoute();
    const tab = ref('information');

    // state open dialog
    const isOpenAddAccountBankDialog: Ref<boolean> = ref(false);
    const isOpenEditAccountBankDialog: Ref<boolean> = ref(false);
    const openAddHotProduct: Ref<boolean> = ref(false);
    const openUpdateHotProduct: Ref<boolean> = ref(false);
    const isOpenDialogEmbed: Ref<boolean> = ref(false);
    const isOpenAddStory: Ref<boolean> = ref(false);
    const isOpenUpdateStory: Ref<boolean> = ref(false);

    // state
    const fieldOptions: Ref<FieldType[]> = ref([]);
    const nationalityOptions: Ref<NationalityType[]> = ref([]);
    const professionOptions: Ref<QualificationType[]> = ref([]);
    const artistLevelOptions: Ref<ArtistLevelType[]> = ref([]);
    const cardBankOptions: Ref<CardBankType[]> = ref([]);
    const typeBankOptions: Ref<TypeCardType[]> = ref([]);
    const workOptions: Ref<WorkType[]> = ref([]);
    const musicOptions: Ref<MusicType[]>= ref([]);
    const provinceOptions:Ref<ProvinceType[]> = ref([]);
    const sexOptions = ref([
      { id: 1, name: 'Nam' },
      { id: 2, name: 'Nữ' },
      { id: 3, name: 'Khác' },
    ]);

    //state sử dụng trong tab thong-tin-ca-nhan
    const id: Ref<number> = ref(0);
    const account: Ref<string | null> = ref(null);
    const avatar: Ref<string | null> = ref(null);
    const avatarFile: Ref<File | null> = ref(null);
    const avatarUploaded: Ref<string | null> = ref(null);
    const artistCode: Ref<string> = ref('');
    const fullName: Ref<string> = ref('');
    const artistName: Ref<string | null> = ref(null);
    const birthday: Ref<string | null> = ref(null);
    const sex: Ref<number | null> = ref(null);
   
    const nationality: Ref<NationalityType> = ref({
      id: 1,
      name: 'Việt Nam',
      numIndex: 1,
      status: 1,
    });
    const favoriteScore: Ref<number | undefined> = ref(undefined);
    const mnName: Ref<string | null> = ref(null);
    const mnPhone: Ref<string | null> = ref(null);
    const mnEmail: Ref<string | null> = ref(null);
    const qualification: Ref<QualificationType | null> = ref(null);
    const artistLevel: Ref<ArtistLevelType | null> = ref(null);
    const address: Ref<ProvinceType | null> = ref(null);
    const status: Ref<number> = ref(1);
    const fields: Ref<FieldType | null> = ref(null);
    const musics: Ref<MusicType | null> = ref(null);
    const works: Ref<WorkType[]> = ref([]);
    const phoneNumber: Ref<string | null> = ref(null);
    const email: Ref<string | null> = ref(null);
    const mnBookingEmail: Ref<string | null> = ref(null);
    const mnBookingPhone: Ref<string | null> = ref(null);
    const mnFbmess: Ref<string | null> = ref(null);
    const mnIns: Ref<string | null> = ref(null);
    const mnWhatsapp: Ref<string | null> = ref(null);
    const facebook: Ref<string | null> = ref(null);
    const facebookMessage: Ref<string | null | undefined> = ref();
    const instagram: Ref<string | null | undefined> = ref();
    const whatsapp: Ref<string | null | undefined> = ref();
    // const hidden_img = ref(false);

    // state sử dụng trong tab vab account
    const banners: Ref<BannerType[]> = ref([]);
    const shortDescription: Ref<string | null> = ref('');
    const socialEmbedded: Ref<string | null> = ref(null);
    const stories: Ref<StoriesType[]> = ref([]);
    const schedules: Ref<SchedulesType[]> = ref([]);
    const formatSchedules: Ref<string[]> = ref([]);

    //state accountBank
    const bankAccounts: Ref<BankAccountType[]> = ref([]);
    const rowBankAccIdx: Ref<number> = ref(0);
    const rowDataAccBank: Ref<BankAccountType | null> = ref(null);

    //state hot product
    const products: Ref<ProductType[]> = ref([]);
    const statusHotProduct: Ref<number> = ref(2);
    const DataInsertHotProduct: Ref<ProductType | null> = ref(null);
    const DataUpdateHotProduct: Ref<ProductType | null> = ref(null);

    //sate error input thong-tin-ca-nhan
    const check_infoBooking = ref(false);
    const artistCodeRules = ref(false);
    const fullNameRules = ref(false);
    // const artistNameRules = ref(false);
    // const birthdayRules = ref(false);
    // const emailRules = ref(false);
    // const addressRules = ref(false);
    // const phoneNumberRules = ref(false);
    // const mnBookingEmailRules = ref(false);
    // const mnBookingPhoneRules = ref(false);
    // const sexRules = ref(false);
    // const nationalityRules = ref(false);
    // const fieldRules = ref(false);
    // const workRules = ref(false);
    // const musicRules = ref(false);
    // const qualificationRules = ref(false);
    // const artistLevelRules = ref(false);
   const accountRules=ref(false)

    // const errorMessEmail = ref( 
    //   i18n.global.t('artist.artistInformation.validateMessages.requireEmail')
    // );
    // const errorMessPhoneNumber = ref(
    //   i18n.global.t(
    //     'artist.artistInformation.validateMessages.requirePhoneNumber'
    //   )
    // );
    
    // const errorMessmnBookingEmail = ref(
    //   i18n.global.t(
    //     'artist.artistInformation.validateMessages.requireEmailAdminister'
    //   )
    // );
    // const errorMessAddress = ref(i18n.global.t('artist.artistInformation.validateMessages.requireAddress'))
    const errorMessAccount = ref(
      i18n.global.t(
        'artist.artistInformation.validateMessages.requireAccount')
    );
    // const errorMessmnBookingPhone = ref(
    //   i18n.global.t(
    //     'artist.artistInformation.validateMessages.requirePhoneNumberAdminister'
    //   )
    // );
    watch(
      () => fields.value,
      (value) => {
        if (value !== null) {
          void getWorkOptions();
          works.value = [];
        } else {
          workOptions.value = [];
          works.value = [];
        }
      }
    );
    watch(
      () => artistCode.value,
      (value) => {
        if (value) {
          artistCodeRules.value = false;
        }
      }
    );
    watch(
      () => account.value,
      (value) => {
        if(value){
          accountRules.value = false
        }
      }
    );
    watch(
      () => fullName.value,
      (value) => {
        if (value) {
          fullNameRules.value = false;
        }
      }
    );
    // watch(
    //   () => artistName.value,
    //   (value) => {
    //     if (value) {
    //       artistNameRules.value = false;
    //     }
    //   }
    // );
    // watch(
    //   () => birthday.value,
    //   (value) => {
    //     if (value) {
    //       birthdayRules.value = false;
    //     }
    //   }
    // );
    // watch(
    //   () => email.value,
    //   (value) => {
    //     if (value) {
    //       emailRules.value = false;
    //     }
    //   }
    // );
    // watch(
    //   () => address.value,
    //   (value) => {
    //     if (value) {
    //       addressRules.value = false;
    //     }
    //   }
    // );
    // watch(
    //   () => phoneNumber.value,
    //   (value) => {
    //     if (value) {
    //       phoneNumberRules.value = false;
    //     }
    //   }
    // );
    // watch(
    //   () => mnBookingPhone.value,
    //   (value) => {
    //     if (value) {
    //       mnBookingPhoneRules.value = false;
    //     }
    //   }
    // );
    // watch(
    //   () => mnBookingEmail.value,
    //   (value) => {
    //     if (value) {
    //       mnBookingEmailRules.value = false;
    //     }
    //   }
    // );
    // watch(
    //   () => sex.value,
    //   (value) => {
    //     if (value) {
    //       sexRules.value = false;
    //     }
    //   }
    // );
    // watch(
    //   () => nationality.value,
    //   (value) => {
    //     if (value) {
    //       nationalityRules.value = false;
    //     }
    //   }
    // );
    // watch(
    //   () => fields.value,
    //   (value) => {
    //     if (value) {
    //       fieldRules.value = false;
    //     }
    //   }
    // );
    // watch(
    //   () => works.value,
    //   (value) => {
    //     if (value) {
    //       workRules.value = false;
    //     }
    //   }
    // );
    // watch(
    //  () => musics.value,
    //   (value) => {
    //     if(value) {
    //       musicRules.value = false;
    //     }
    //   }
    // );
    // watch(
    //   () => qualification.value,
    //   (value) => {
    //     if (value) {
    //       qualificationRules.value = false;
    //     }
    //   }
    // );
    // watch(
    //   () => artistLevel.value,
    //   (value) => {
    //     if (value) {
    //       artistLevelRules.value = false;
    //     }
    //   }
    // );

    const getFieldOptions = async () => {
      const response = (await api({
        url: API_PATHS.getFieldOptions,
        method: 'GET',
        params: {},
      })) as AxiosResponse<BaseResponseBody<FieldType[]>>;
      if (response.data.error.code === config.API_RES_CODE.OK.code) {
        fieldOptions.value = response.data.data;
      }
    };

    const getNationalityOptions = async () => {
      const response = (await api({
        url: API_PATHS.getNationalityOptions,
        method: 'GET',
        params: {},
      })) as AxiosResponse<BaseResponseBody<NationalityType[]>>;
      if (response.data.error.code === config.API_RES_CODE.OK.code) {
        nationalityOptions.value = response.data.data;
      }
    };

    const getProvinceOptions = async () => {
      const response = (await api ({
        url : API_PATHS.getProvinceOptions,
        method: 'GET',
        params: {},
      })) as AxiosResponse<BaseResponseBody<ProvinceType[]>>;
      if(response.data.error.code === config.API_RES_CODE.OK.code){
        provinceOptions.value = response.data.data
      }
   } 

    const getArtistLevelOptions = async () => {
      const response = (await api({
        url: API_PATHS.getArtistLevelOptions,
        method: 'GET',
        params: {},
      })) as AxiosResponse<BaseResponseBody<ArtistLevelType[]>>;
      if (response.data.error.code === config.API_RES_CODE.OK.code) {
        artistLevelOptions.value = response.data.data;
      }
    };
    const getQualificationOptions = async () => {
      const response = (await api({
        url: API_PATHS.getQualificationOptions,
        method: 'GET',
        params: {},
      })) as AxiosResponse<BaseResponseBody<QualificationType[]>>;
      if (response.data.error.code === config.API_RES_CODE.OK.code) {
        professionOptions.value = response.data.data;
      }
    };
    const getWorkOptions = async () => {
      const response = (await api({
        url: API_PATHS.getWorkOptions,
        method: 'GET',
        params: {
          fieldId: fields.value?.id,
        },
      })) as AxiosResponse<BaseResponseBody<WorkType[]>>;
      if (response.data.error.code === config.API_RES_CODE.OK.code) {
        workOptions.value = response.data.data;
      }
    };

     // thể loại
     const getMusicTypeOptions = async () => {
      const response = (await api ({
        url: API_PATHS.getMusicTypeOptions,
        method: 'GET',
        params: {},
      })) as AxiosResponse<BaseResponseBody<MusicType[]>>;
      if(response.data.error.code === config.API_RES_CODE.OK.code) {
        musicOptions.value = response.data.data
      }

     };

    const getBankOptions = async () => {
      const response = (await api({
        url: API_PATHS.bankOptions,
        method: 'GET',
        params: {},
      })) as AxiosResponse<BaseResponseBody<CardBankType[]>>;
      if (response.data.error.code === config.API_RES_CODE.OK.code) {
        cardBankOptions.value = response.data.data;
      }
    };
    const getTypeCardOptions = async () => {
      const response = (await api({
        url: API_PATHS.cardTypeOptions,
        method: 'GET',
        params: {},
      })) as AxiosResponse<BaseResponseBody<TypeCardType[]>>;
      if (response.data.error.code === config.API_RES_CODE.OK.code) {
        typeBankOptions.value = response.data.data;
      }
    };

    const addAccBank = (item: BankAccountType) => {
      if (item.isDefault === 1) {
        bankAccounts.value.forEach((element) => (element.isDefault = 2));
        bankAccounts.value.push(item);
        isOpenAddAccountBankDialog.value = false;
      } else {
        bankAccounts.value.push(item);
        isOpenAddAccountBankDialog.value = false;
      }
    };
    const editAccBank = (item: BankAccountType) => {
      if (item.isDefault === 1) {
        bankAccounts.value.forEach((element) => (element.isDefault = 2));
        bankAccounts.value[rowBankAccIdx.value] = item;
        isOpenEditAccountBankDialog.value = false;
      } else {
        bankAccounts.value[rowBankAccIdx.value] = item;
        isOpenEditAccountBankDialog.value = false;
      }
    };
    const confirmDeleteAccBank = (value: number) => {
      Dialog.create({
        title: i18n.global.t(
          'artist.bankAccount.confirmActionsTitle.confirmDeleteAccBankTitle'
        ),
        message: i18n.global.t(
          'artist.bankAccount.confirmActionsTitle.confirmDeleteAccBankContent'
        ),
        cancel: i18n.global.t(
          'artist.bankAccount.confirmActionsTitle.confirmDeleteAccBankBtnLabel'
        ),
        color: 'negative',
      }).onOk(() => {
        Notify.create({
          type: 'positive',
          message: i18n.global.t(
            'artist.dialogLabel.validateMessages.deleteAccess'
          ),
          actions: [{ icon: 'close', color: 'white' }],
        });
        bankAccounts.value.splice(value, 1);
      });
    };
    const openDialogEditAccBank = (
      itemData: BankAccountType,
      itemIdx: number
    ) => {
      rowDataAccBank.value = itemData;
      rowBankAccIdx.value = itemIdx;
      isOpenEditAccountBankDialog.value = true;
    };

    const pushData = (value: ProductType) => {
      products.value.push(value);
    };

    const selectedFile = (value: BannerType) => {
      banners.value.push(value);
    };

    const editBanner = (data: { index: number; obj: BannerType }) => {
      banners.value[data.index] = data.obj;
    };
    const reset = ref(null);
    const resetOldData = () => {
      document.getElementById('reset')?.click();
    };
    const confirmDeleteRow = (value: number) => {
      Dialog.create({
        title: i18n.global.t(
          'artist.hotProduct.confirmActionsTitle.confirmDeleteHotProdcutTitle'
        ),
        message: i18n.global.t(
          'artist.hotProduct.confirmActionsTitle.confirmDeleteHotProdcutContent'
        ),
        cancel: i18n.global.t(
          'artist.hotProduct.confirmActionsTitle.confirmDeleteHotProdcutBtnLabel'
        ),
        color: 'negative',
      }).onOk(() => {
        Notify.create({
          type: 'positive',
          message: i18n.global.t(
            'artist.dialogLabel.validateMessages.deleteAccess'
          ),
          actions: [{ icon: 'close', color: 'white' }],
        });
        products.value.splice(value, 1);
      });
    };
    const UpdateData = (value: ProductType) => {
      for (let i = 0; i < products.value.length; i++) {
        if (products.value[i].id === value?.id) {
          products.value[i] = value;
        }
      }
    };
    const SetProduct = (value: ProductType) => {
      DataUpdateHotProduct.value = value;
    };
    const setAvatar = (value: BannerType) => {
      avatarFile.value = value.file as File;
      avatar.value = value.url as string;
    };

    const confirmDeleteSocialEmbedded = () => {
      Dialog.create({
        title: i18n.global.t(
          'artist.bankAccount.confirmActionsTitle.confirmDeleteAccBankTitle'
        ),
        message: i18n.global.t(
          'artist.confirmActionsTitle.confirmDeleteEmbedContent'
        ),
        cancel: i18n.global.t(
          'artist.bankAccount.confirmActionsTitle.confirmDeleteAccBankBtnLabel'
        ),
        color: 'negative',
      }).onOk(() => {
        socialEmbedded.value = null;
        Notify.create({
          type: 'positive',
          message: i18n.global.t('artist.actionMessages.EmbedDeleteSuccess'),
          actions: [{ icon: 'close', color: 'white' }],
        });
      });
    };

    const changeEmbed = (newEmbed: string | null) => {
      socialEmbedded.value = newEmbed;
      isOpenDialogEmbed.value = false;
    };

    const addStory = (value: StoriesType) => {
      stories.value.push(value);
    };

    const deleteStory = (idx: number) => {
      Dialog.create({
        title: i18n.global.t(
          'artist.bankAccount.confirmActionsTitle.confirmDeleteAccBankTitle'
        ),
        message: i18n.global.t(
          'artist.vabAccount.confirmActionsTitle.confirmDeleteStory'
        ),
        cancel: i18n.global.t(
          'artist.bankAccount.confirmActionsTitle.confirmDeleteAccBankBtnLabel'
        ),
        color: 'negative',
      }).onOk(() => {
        Notify.create({
          type: 'positive',
          message: i18n.global.t(
            'artist.dialogLabel.validateMessages.deleteStoryAccess'
          ),
          actions: [{ icon: 'close', color: 'white' }],
        });
        stories.value.splice(idx, 1);
      });
    };
    const UpdateBirtday = (value: string) => {
      birthday.value = value;
    };
    const callApiUploadAvatar = async (file: File) => {
      try {
        const bodyFormData = new FormData();
        bodyFormData.append('file', file);
        const response = (await api({
          headers: { 'Content-Type': 'multipart/form-data' },
          url: config.API_IMAGE_ENDPOINT,
          method: 'POST',
          data: bodyFormData,
        })) as AxiosResponse<BaseResponseBody<FileUploadType>>;
        if (response.data.error.code === config.API_RES_CODE.OK.code) {
          avatarUploaded.value = response.data.data.fileName;
        }
      } catch (error) {}
    };
    const callApiUploadBanners = async (file: File, index: number) => {
      try {
        const bodyFormData = new FormData();
        bodyFormData.append('file', file);
        const response = (await api({
          headers: { 'Content-Type': 'multipart/form-data' },
          url: config.API_IMAGE_ENDPOINT,
          method: 'POST',
          data: bodyFormData,
        })) as AxiosResponse<BaseResponseBody<FileUploadType>>;
        if (response.data.error.code === config.API_RES_CODE.OK.code) {
          const bannerUpload = response.data.data.fileName;
          banners.value[index] = {
            bannerUrl: bannerUpload,
          };
        }
      } catch (error) {}
    };
    const callApiUploadStories = async (file: File, idxStory: number) => {
      try {
        const bodyFormData = new FormData();
        bodyFormData.append('file', file);
        const response = (await api({
          headers: { 'Content-Type': 'multipart/form-data' },
          url: config.API_IMAGE_ENDPOINT,
          method: 'POST',
          data: bodyFormData,
        })) as AxiosResponse<BaseResponseBody<FileUploadType>>;
        if (response.data.error.code === config.API_RES_CODE.OK.code) {
          const urlStoryUpload = response.data.data.fileName;
          stories.value[idxStory].imageUrl = urlStoryUpload;
        }
      } catch (error) {}
    };

    const callAPIUploadHotProduct = async (file: File, idx: number) => {
      try {
        const bodyFormData = new FormData();
        bodyFormData.append('file', file);
        const response = (await api({
          headers: { 'Content-Type': 'multipart/form-data' },
          url: config.API_IMAGE_ENDPOINT,
          method: 'POST',
          data: bodyFormData,
        })) as AxiosResponse<BaseResponseBody<FileUploadType>>;
        if (response.data.error.code === config.API_RES_CODE.OK.code) {
          const urlHotProductUpload = response.data.data.fileName;
          products.value[idx].imageUrl = urlHotProductUpload;
        }
      } catch (error) {}
    };
    const checkValidate = () => {
      let hasError = false;
      // if (!avatarFile.value) {
      //   hasError = true;
      //   hidden_img.value = true;
      // }
      if (!artistCode.value || !artistCode.value?.trim().length) {
        hasError = true;
        artistCodeRules.value = true;
      }
      if (!fullName.value || !fullName.value?.trim().length) {
        hasError = true;
        fullNameRules.value = true;
      }
      // if (!artistName.value || !artistName.value?.trim().length) {
      //   hasError = true;
      //   artistNameRules.value = true;
      // }
      // if (!birthday.value || !birthday.value?.trim().length) {
      //   hasError = true;
      //   birthdayRules.value = true;
      // }
      // if (sex.value === null) {
      //   hasError = true;
      //   sexRules.value = true;
      // }
      // if (!phoneNumber.value || !phoneNumber.value?.trim().length) {
      //   hasError = true;
      //   phoneNumberRules.value = true;
      //   errorMessPhoneNumber.value = i18n.global.t(
      //     'artist.artistInformation.validateMessages.requirePhoneNumber'
      //   );
      // }

      // if (!email.value || !email.value?.trim().length) {
      //   hasError = true;
      //   emailRules.value = true;
      //   errorMessEmail.value = i18n.global.t(
      //     'artist.artistInformation.validateMessages.requireEmail'
      //   );
      // }
     
      // if (
      //   (!mnBookingPhone.value || !mnBookingPhone.value?.trim().length) &&
      //   check_infoBooking.value
      // ) {
      //   hasError = true;
      //   mnBookingPhoneRules.value = true;
      //   errorMessmnBookingPhone.value = i18n.global.t(
      //     'artist.artistInformation.validateMessages.requirePhoneNumberAdminister'
      //   );
      // }
      // if (
      //   (!mnBookingEmail.value || !mnBookingEmail.value?.trim().length) &&
      //   check_infoBooking.value
      // ) {
      //   hasError = true;
      //   mnBookingEmailRules.value = true;
      //   errorMessmnBookingEmail.value = i18n.global.t(
      //     'artist.artistInformation.validateMessages.requireEmailAdminister'
      //   );
      // }


      // if (!fields.value) {
      //   hasError = true;
      //   fieldRules.value = true;
      // }
      if(!account.value){
        hasError = true;
        accountRules.value = true;
      }
      // if(!address.value){
      //   hasError = true;
      //   addressRules.value = true
      // }
      // if (nationality.value === null) {
      //   hasError = true;
      //   nationalityRules.value = true;
      // }
      // if (qualification.value === null) {
      //   hasError = true;
      //   qualificationRules.value = true;
      // }
      // if (!works.value.length) {
      //   hasError = true;
      //   workRules.value = true;
      // }


      if (hasError === false) {
        void addArtist();
      }
    };
    const addArtist = async () => {
      try {
        if (avatarFile.value !== null) {
          await callApiUploadAvatar(avatarFile.value);
        } else avatarUploaded.value = avatar.value;
        for (let index = 0; index < banners.value.length; index++) {
          const element = banners.value[index];
          if (element.file !== undefined) {
            await callApiUploadBanners(element.file, index);
          }
        }
        for (let idx = 0; idx < stories.value.length; idx++) {
          const item = stories.value[idx];
          if (item.file !== undefined) {
            await callApiUploadStories(item.file, idx);
          }
        }
        for (let idx = 0; idx < products.value.length; idx++) {
          const item = products.value[idx];
          if (item.file !== undefined) {
            await callAPIUploadHotProduct(item.file, idx);
          }
        }

        // cách 1: dùng reduce
        const schedules: Array<{ scheduleTime: string }> =
          formatSchedules.value.reduce(
            (acc: Array<{ scheduleTime: string }>, item) => {
              acc.push({
                scheduleTime: moment(item, 'YYYY/MM/DD').format(
                  'DD/MM/YYYY HH:mm:ss'
                ),
              });
              return acc;
            },
            []
          );
           
         

        const response = (await api({
          url: API_PATHS.addArtist,
          method: 'POST',
          data: {
            musicTypeDto:musics.value  ,
            id: route.params.id,
            avatar: avatarUploaded.value,
            artistCode: artistCode.value,
            artistName: artistName.value,
            // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
            birthday: birthday.value === null ? null :  birthday.value + ' 00:00:00',
            sex: sex.value,
            provinceDto: address.value,
            address: null,
            phoneNumber: phoneNumber.value,
            email: email.value,
            facebook: facebook.value,
            facebookMessage: facebookMessage.value,
            instagram: instagram.value,
            whatsapp: whatsapp.value,
            fullName: fullName.value,
            // "workStatus": 1,
            shortDescription: null,
            account: account.value,
            socialEmbedded: socialEmbedded.value,
            // artistLevel: artistLevel.value,
            fields: fields.value,
            nationality: nationality.value,
            qualification: qualification.value,
            works: works.value,
            banners: banners.value,
            bankAccounts: bankAccounts.value,
            schedules,
            stories: stories.value,
            products: products.value,
            mnName: mnName.value,
            mnPhone: mnPhone.value,
            mnEmail: mnEmail.value,
            mnBookingPhone: mnBookingPhone.value,
            mnBookingEmail: mnBookingEmail.value,
            mnFbmess: mnFbmess.value,
            mnIns: mnIns.value,
            mnWhatsapp: mnWhatsapp.value,
            favoriteScore: favoriteScore.value,
          },
        })) as AxiosResponse<BaseResponseBody<unknown>>;
        if (response.data.error.code === config.API_RES_CODE.OK.code) {
          await Router.push({ name: Pages.artist });
          Notify.create({
            type: 'positive',
            message: i18n.global.t('artist.actionMessages.addNewArtistAccess'),
            actions: [{ icon: 'close', color: 'white' }],
          });
        } else {
          console.log(response.data.error.code);
        }
      } catch (error) {
        
      }
    };
    const DeleteItemStories = (value: StoriesType) => {
      for (let i = 0; i < stories.value.length; i++) {
        if (stories.value[i].id == value.id) {
          stories.value.splice(Number(stories.value[i]), 1);
        }
      }
    };

    const deleteAvatar = () => {
      avatar.value = null;
      avatarFile.value = null;
    };
    const confirmChangeIsDefault = (item: changeIsDefault) => {
      if (item.isDefault === 2) {
        bankAccounts.value.map((item) => (item.isDefault = 2));
        bankAccounts.value[item.idxAcc].isDefault = 1;
      } else bankAccounts.value[item.idxAcc].isDefault = 2;
    };
    onMounted(() => {
      void getFieldOptions();
      void getNationalityOptions();
      void getArtistLevelOptions();
      void getQualificationOptions();
      void getBankOptions();
      void getTypeCardOptions();
      void getProvinceOptions();
      void getMusicTypeOptions()
    });
    return {

      // hidden_img,
      tab,
      id,
      route,
      artistCode,
      fullName,
      artistName,
      birthday,
      sex,
      nationality,
      address,
      status,
      fields,
      works,
     musics,

      qualification,
      artistLevel,
      phoneNumber,
      email,
      facebook,
      facebookMessage,
      instagram,
      whatsapp,
      mnName,
      mnPhone,
      mnEmail,
      mnBookingPhone,
      mnBookingEmail,
      mnFbmess,
      mnIns,
      mnWhatsapp,
      // mnBookingPhoneRules,
      // errorMessmnBookingPhone,
      // mnBookingEmailRules,
      // errorMessmnBookingEmail,
      socialEmbedded,
      sexOptions,
      fieldOptions,
      nationalityOptions,
      professionOptions,
      artistLevelOptions,
      workOptions,
      musicOptions,
    
      getFieldOptions,
      getNationalityOptions,
      getArtistLevelOptions,
      getQualificationOptions,
      getWorkOptions,
      bankAccounts,
      products,
      account,
      banners,
      shortDescription,
      stories,
      isOpenAddAccountBankDialog,
      openAddHotProduct,
      addAccBank,
      schedules,
      formatSchedules,
      confirmDeleteAccBank,
      selectedFile,
      editBanner,
      isOpenDialogEmbed,
      cardBankOptions,
      typeBankOptions,
      isOpenEditAccountBankDialog,
      openDialogEditAccBank,
      editAccBank,
      rowBankAccIdx,
      statusHotProduct,
      DataInsertHotProduct,
      pushData,
      resetOldData,
      reset,
      confirmDeleteRow,
      addArtist,
      openUpdateHotProduct,
      SetProduct,
      DataUpdateHotProduct,
      UpdateData,
      getBankOptions,
      getTypeCardOptions,
      rowDataAccBank,
      avatar,
      setAvatar,
      deleteAvatar,
      DeleteItemStories,
      confirmDeleteSocialEmbedded,
      changeEmbed,
      isOpenAddStory,
      isOpenUpdateStory,
      addStory,
      deleteStory,
      UpdateBirtday,
      checkValidate,
      artistCodeRules,
      fullNameRules,
      // artistNameRules,
      // birthdayRules,
      // emailRules,
      // addressRules,
      // phoneNumberRules,
      // sexRules,
      // nationalityRules,
      // fieldRules,
      // workRules,
      // musicRules,
      // qualificationRules,
      // artistLevelRules,
      // errorMessEmail,
      accountRules,
      // addressRules,
      // errorMessPhoneNumber,
      errorMessAccount,
      // errorMessAddress,
      confirmChangeIsDefault,
      favoriteScore,
      check_infoBooking,
      getProvinceOptions,
      provinceOptions,
      getMusicTypeOptions,
    };
  },
});
