import { i18n } from 'src/boot/i18n';
import { defineComponent, onMounted, Ref, ref } from 'vue';
import { API_PATHS, config } from 'src/assets/configurations.example';
import { AxiosResponse } from 'axios';
import { api, BaseResponseBody } from 'src/boot/axios';
import {
  CategoryPostType,
  LanguageType,
  PostCategoryDetailType,
} from 'src/assets/type';
import AddCategoryPostDialog from 'components/post-category/add-post-category/index.vue';
import UpdateCategoryPostDialog from 'components/post-category/update-post-category/index.vue';
import { Dialog, Notify } from 'quasar';
// import { Dialog, Notify } from 'quasar';
export default defineComponent({
  components: {
    AddCategoryPostDialog,
    UpdateCategoryPostDialog,
  },
  setup() {
    const date = ref('');
    const configImg = config;
    const keywordSearch: Ref<string | null> = ref(null);
    const userTableColumnsCategoryPost = [
      {
        name: 'stt',
        field: 'stt',
        required: true,
        label: i18n.global.t('postCategory.tableColumnsPostCategory.stt'),
        align: 'center',
        sortable: false,
      },
      {
        name: 'name',
        field: 'name',
        required: true,
        label: i18n.global.t('postCategory.tableColumnsPostCategory.name'),
        align: 'center',
        headerStyle: 'text-align: center !important;',
        sortable: false,
      },
      {
        name: 'image',
        field: 'image',
        required: true,
        label: i18n.global.t('postCategory.tableColumnsPostCategory.image'),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'title',
        field: 'title',
        required: true,
        label: i18n.global.t('postCategory.tableColumnsPostCategory.title'),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'status',
        field: 'status',
        required: true,
        label: i18n.global.t('postCategory.tableColumnsPostCategory.status'),
        align: 'center',
        sortable: false,
      },
      {
        name: 'action',
        field: 'action',
        required: true,
        label: i18n.global.t('postCategory.tableColumnsPostCategory.action'),
        align: 'center',
        sortable: false,
      },
    ];
    const userTableRowsCategoryPost: Ref<CategoryPostType[]> = ref([]);
    const isOpenAddDialog: Ref<boolean> = ref(false);
    const isOpenUpdateDialog: Ref<boolean> = ref(false);
    const languages: Ref<LanguageType[]> = ref([]);
    const detailPostCategory: Ref<PostCategoryDetailType | null> = ref(null);
    const getListCategoryPost = async () => {
      try {
        const response = (await api({
          url: API_PATHS.getListCategoryPost,
          method: 'GET',
          params: {
            name: keywordSearch.value?.trim(),
          },
        })) as AxiosResponse<BaseResponseBody<CategoryPostType[]>>;
        if (response.data.error.code === config.API_RES_CODE.OK.code) {
          userTableRowsCategoryPost.value = response.data.data;
          keywordSearch.value = null;
        }
      } catch (error) {}
    };
    // const getList = () => {
    //   return new Promise((resolve, reject) => {
    //     void api({
    //       url: API_PATHS.getListCategoryPost,
    //       method: 'GET',
    //       params: {
    //         name: keywordSearch.value,
    //       },
    //     }),
    //       (
    //         err: any,
    //         result: AxiosResponse<BaseResponseBody<CategoryPostType[]>>
    //       ) => {
    //         if (err) return reject(err);
    //         resolve(result);
    //       };
    //   });
    // };
    const getLanguages = async () => {
      try {
        const response = (await api({
          url: API_PATHS.getLanguage,
          method: 'GET',
          params: {},
        })) as AxiosResponse<BaseResponseBody<LanguageType[]>>;
        if (response.data.error.code === config.API_RES_CODE.OK.code) {
          languages.value = response.data.data;
        }
      } catch (error) {}
    };

    const addPostCategory = async (formData: PostCategoryDetailType) => {
      try {
        const response = (await api({
          url: API_PATHS.addCategoryPost,
          method: 'POST',
          data: {
            image: formData.image,
            status: formData.status,
            posts: formData.posts,
            langs: formData.langs,
          },
        })) as AxiosResponse<BaseResponseBody<unknown>>;
        if (response.data.error.code === config.API_RES_CODE.OK.code) {
          isOpenAddDialog.value = false;
          Notify.create({
            type: 'positive',
            message: i18n.global.t(
              'postCategory.actionMessages.addPostCategoryAccess'
            ),
            actions: [{ icon: 'close', color: 'white' }],
          });
          void getListCategoryPost();
        }
      } catch (error) {}
    };

    // const confirmDelete = (id: number) => {
    //   Dialog.create({
    //     title: i18n.global.t(
    //       'artist.bankAccount.confirmActionsTitle.confirmDeleteAccBankTitle'
    //     ),
    //     message: i18n.global.t(
    //       'postCategory.confirmActionsTitle.confirmDeletePostCategory'
    //     ),
    //     cancel: i18n.global.t(
    //       'artist.bankAccount.confirmActionsTitle.confirmDeleteAccBankBtnLabel'
    //     ),
    //     color: 'negative',
    //   }).onOk(async () => {
    //     try {
    //       const response = (await api({
    //         url: API_PATHS.deleteCategoryPost,
    //         method: 'GET',
    //         params: {
    //           id: id,
    //         },
    //       })) as AxiosResponse<BaseResponseBody<unknown>>;
    //       if (response.data.error.code === config.API_RES_CODE.OK.code) {
    //         Notify.create({
    //           type: 'positive',
    //           message: i18n.global.t(
    //             'postCategory.actionMessages.deletePostCategoryAccess'
    //           ),
    //           actions: [{ icon: 'close', color: 'white' }],
    //         });
    //         void getListCategoryPost();
    //       }
    //     } catch (error) {}
    //   });
    // };
    const openUpdateDialog = async (id: number) => {
      await getDetailCategoryPost(id);
      isOpenUpdateDialog.value = true;
    };
    const getDetailCategoryPost = async (id: number) => {
      try {
        const response = (await api({
          url: API_PATHS.detailCategoryPost,
          method: 'GET',
          params: {
            id: id,
          },
        })) as AxiosResponse<BaseResponseBody<PostCategoryDetailType>>;
        if (response.data.error.code === config.API_RES_CODE.OK.code) {
          detailPostCategory.value = response.data.data;
        }
      } catch (error) {}
    };

    const updatePostCategory = async (formData: PostCategoryDetailType) => {
      try {
        const response = (await api({
          url: API_PATHS.updateCategoryPost,
          method: 'POST',
          data: {
            id: formData.id,
            image: formData.image,
            status: formData.status,
            posts: formData.posts,
            langs: formData.langs,
          },
        })) as AxiosResponse<BaseResponseBody<unknown>>;
        if (response.data.error.code === config.API_RES_CODE.OK.code) {
          isOpenUpdateDialog.value = false;
          Notify.create({
            type: 'positive',
            message: i18n.global.t(
              'postCategory.actionMessages.updatePostCategoryAccess'
            ),
            actions: [{ icon: 'close', color: 'white' }],
          });
          void getListCategoryPost();
        }
      } catch (error) {}
    };

    onMounted(() => {
      void getListCategoryPost();
      void getLanguages();
    });
    return {
      date,
      configImg,
      keywordSearch,
      userTableColumnsCategoryPost,
      userTableRowsCategoryPost,
      isOpenAddDialog,
      isOpenUpdateDialog,
      getListCategoryPost,
      getLanguages,
      languages,
      addPostCategory,
      // confirmDelete,
      openUpdateDialog,
      detailPostCategory,
      updatePostCategory,
    };
  },
});
