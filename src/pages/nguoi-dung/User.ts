/* eslint-disable @typescript-eslint/ban-types */
import { AxiosResponse } from 'axios';
import { Dialog, Notify } from 'quasar';
import { API_PATHS, config } from 'src/assets/configurations.example';
import { PaginationResponse, UserObject } from 'src/assets/type';
import { api, BaseResponseBody } from 'src/boot/axios';
import { i18n } from 'src/boot/i18n';
import { useStore } from '../../store/index';
import { defineComponent, onMounted, ref, Ref } from 'vue';
import AddNewUserDialogComponent from '../../components/user-management/add-new-user-dialog/index.vue';
import UpdateUserDialogComponent from '../../components/user-management/update-user-dialog/index.vue';
import { GroupInfoType } from '../nhom-nguoi-dung/UserGroup';
import Pagination from 'components/pagination/index.vue';
// import moment from 'moment';

export default defineComponent({
  components: {
    AddNewUserDialogComponent,
    UpdateUserDialogComponent,
    Pagination,
  },
  setup() {
    const totalPage = ref(0);
    const pageIndex = ref(1);
    const pageSize = ref(20);
    const userTableColumns = [
      {
        name: 'userName',
        field: 'userName',
        required: true,
        label: i18n.global.t('userPage.tableColumns.userName'),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'fullName',
        field: 'fullName',
        required: true,
        label: i18n.global.t('userPage.tableColumns.fullName'),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'email',
        field: 'email',
        required: false,
        label: i18n.global.t('userPage.tableColumns.email'),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'mobileNumber',
        field: 'mobileNumber',
        required: true,
        label: i18n.global.t('userPage.tableColumns.phone'),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'unit',
        field: 'unit',
        required: true,
        label: i18n.global.t('userPage.tableColumns.unit'),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'status',
        field: 'status',
        required: true,
        label: i18n.global.t('userPage.tableColumns.status'),
        headerStyle: 'text-align: center !important;',
        align: 'center',
        sortable: false,
      },
      {
        name: 'action',
        field: 'action',
        required: true,
        label: i18n.global.t('userPage.tableColumns.action'),
        headerStyle: 'text-align: center !important;',
        align: 'center',
        sortable: false,
      },
    ];
    const userTableRows: Ref<UserObject[]> = ref([]);
    const keyword = ref('');
    const showDialog = ref(false);
    const showDialogUpdate = ref(false);

    const id = ref(0);
    const userName = ref('');
    const password = ref('');
    const fullName = ref('');
    const birthday: Ref<string | undefined> = ref();
    const email = ref('');
    const phoneNumber = ref('');
    const mobileNumber = ref('');
    const sex: Ref<number | undefined> = ref();
    const sexOptions = ref([
      { id: 1, text: 'Nam' },
      { id: 2, text: 'Nữ' },
      { id: 3, text: 'Khác' },
    ]);
    const address: Ref<string | undefined> = ref();
    const unit: Ref<string | null> = ref(null);
    const status: Ref<boolean | number> = ref(true);
    const listGroup: Ref<{ id: number; groupName: string }[]> = ref([]);
    const group: Ref<GroupInfoType[]> = ref([]);
    const scheduleAccess = ref('Chưa có lịch truy cập');
    const listScheduleAccess = ref(['Chưa có lịch truy cập']);
    const $store = useStore();

    const getListUsers = async () => {
      try {
        const response = (await api({
          url: API_PATHS.getListUsers,
          method: 'GET',
          params: {
            pageIndex: pageIndex.value,
            pageSize: pageSize.value,
            name: keyword.value.trim(),
          },
        })) as AxiosResponse<BaseResponseBody<PaginationResponse<UserObject>>>;
        if (response.data.error.code === config.API_RES_CODE.OK.code) {
          userTableRows.value = response.data.data.data;
          totalPage.value = response.data.data.totalPages;
        }
      } catch (error) {}
    };

    // const filterListUser: Ref<UserObject[]> = computed(() => {
    //   return userTableRows.value.filter((user) =>
    //     user.userName.includes(keyword.value)
    //   );
    // });

    const getListUserGroup = async () => {
      await $store.dispatch('authentication/getListUsers').then((response) => {
        const res = response as AxiosResponse<
          BaseResponseBody<GroupInfoType[]>
        >;
        if (!res.data.error.code) {
          res.data.data.map((item) => {
            listGroup.value.push({ id: item.id, groupName: item.groupName });
          });
        }
      });
    };
    const openAddUserDialog = () => {
      showDialog.value = true;
      userName.value = '';
      password.value = '';
      fullName.value = '';
      birthday.value = '';
      email.value = '';
      phoneNumber.value = '';
      mobileNumber.value = '';
      sex.value = undefined;
      address.value = '';
      unit.value = null;
      status.value = true;
      group.value = [];
    };

    const addNewUser = async () => {
      try {
        const groups: { id: number }[] = [];
        group.value.map((item) => {
          groups.push({ id: item as unknown as number });
        });
        const response = (await api({
          url: API_PATHS.addNewUser,
          method: 'POST',
          data: {
            user: {
              userName: userName.value.trim(),
              password: password.value,
              fullName: fullName.value.trim(),
              // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
              birthday: birthday.value + ' 00:00:00',
              email: email.value.trim(),
              phoneNumber: phoneNumber.value.trim(),
              mobileNumber: mobileNumber.value.trim(),
              sex: sex.value,
              address: address.value?.trim(),
              unit: unit.value?.trim(),
              status: status.value ? 1 : 0,
            },
            groups: groups,
            pageRoles: [],
          },
        })) as AxiosResponse<BaseResponseBody<unknown>>;
        if (response.data.error.code === config.API_RES_CODE.OK.code) {
          showDialog.value = false;
          Notify.create({
            type: 'positive',
            message: i18n.global.t('userPage.actionMessages.addNewUserAccess'),
            actions: [{ icon: 'close', color: 'white' }],
          });

          void getListUsers();
        }
      } catch (error) {}
    };

    const confirmDeleteUser = (userId: number, userName: string) => {
      Dialog.create({
        title: i18n.global.t(
          'userPage.confirmActionsTitle.confirmDeleteUserTitle'
        ),
        message:
          i18n.global.t(
            'userPage.confirmActionsTitle.confirmDeleteUserContent'
          ) + ` "${userName}"`,
        cancel: i18n.global.t(
          'userPage.confirmActionsTitle.confirmDeleteUserCancelBtnLabel'
        ),
        color: 'negative',
      }).onOk(() => {
        void deleteUser(userId);
      });
    };

    const deleteUser = async (userId: number) => {
      try {
        const deleteResult = (await api({
          url: API_PATHS.deleteUser,
          method: 'GET',
          params: {
            userId: userId,
          },
        })) as AxiosResponse<BaseResponseBody<unknown>>;

        if (deleteResult.data.error.code === config.API_RES_CODE.OK.code) {
          Notify.create({
            type: 'positive',
            message: i18n.global.t('userPage.actionMessages.deleteUserAccess'),
            actions: [{ icon: 'close', color: 'white' }],
          });
          void getListUsers();
        }
      } catch (error) {}
    };

    const confirmResetPassword = (userId: number) => {
      Dialog.create({
        title: i18n.global.t(
          'userPage.confirmActionsTitle.confirmDeleteUserTitle'
        ),
        // message: i18n.global.t(
        //   'userPage.confirmActionsTitle.confirmResetPasswordContent'
        // ),
        message:
          'Bạn có muốn reset về mật khẩu "123@123a" cho người dùng này không',
        cancel: i18n.global.t(
          'userPage.confirmActionsTitle.confirmDeleteUserCancelBtnLabel'
        ),
        color: 'negative',
      }).onOk(() => {
        void resetPassword(userId);
      });
    };

    const resetPassword = async (userId: number) => {
      try {
        const deleteResult = (await api({
          url: API_PATHS.resetPassword,
          method: 'GET',
          params: {
            userId: userId,
          },
        })) as AxiosResponse<BaseResponseBody<unknown>>;

        if (deleteResult.data.error.code === config.API_RES_CODE.OK.code) {
          Notify.create({
            type: 'positive',
            message: i18n.global.t(
              'userPage.actionMessages.resetPasswordAccess'
            ),
            actions: [{ icon: 'close', color: 'white' }],
          });
          void getListUsers();
        }
      } catch (error) {}
    };

    const showDialogUpdateUser = (userId: number) => {
      showDialogUpdate.value = true;
      void getUserDetail(userId);
    };

    const getUserDetail = async (userId: number) => {
      try {
        const response = (await api({
          url: API_PATHS.getUserDetail,
          method: 'GET',
          params: {
            userId: userId,
          },
        })) as AxiosResponse<
          BaseResponseBody<{ user: UserObject; groups: GroupInfoType[] }>
        >;
        if (response.data.error.code === config.API_RES_CODE.OK.code) {
          const userInfo = response.data.data.user;
          // const groupInfo = response.data.data.groups;
          group.value = response.data.data.groups;
          address.value = userInfo.address as string;
          id.value = userInfo.id;
          // birthday.value = moment(userInfo.birthday).format('YYYY-MM-DD') as string;
          // birthday.value = moment(
          //   userInfo.birthday,
          //   'DD/MM/YYYY HH:mm:ss'
          // ).format('DD/MM/YYYY');
          email.value = userInfo.email as string;
          fullName.value = userInfo.fullName as string;
          mobileNumber.value = userInfo.mobileNumber as string;
          phoneNumber.value = userInfo.phoneNumber as string;
          sex.value = userInfo.sex;
          status.value = userInfo.status ? true : false;
          unit.value = userInfo.unit;
          userName.value = userInfo.userName;
        }
      } catch (error) {}
    };

    const updateUser = async () => {
      try {
        const response = (await api({
          url: API_PATHS.updateUser,
          method: 'POST',
          data: {
            user: {
              id: id.value,
              userName: userName.value.trim(),
              password: password.value,
              fullName: fullName.value.trim(),
              // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
              birthday: birthday.value + ' 00:00:00',
              email: email.value.trim(),
              phoneNumber: phoneNumber.value.trim(),
              mobileNumber: mobileNumber.value.trim(),
              sex: sex.value,
              address: address.value?.trim(),
              unit: unit.value?.trim(),
              status: status.value ? 1 : 0,
            },
            groups: group.value,
            pageRoles: [],
          },
        })) as AxiosResponse<BaseResponseBody<unknown>>;
        if (response.data.error.code === config.API_RES_CODE.OK.code) {
          showDialogUpdate.value = false;
          Notify.create({
            type: 'positive',
            message: i18n.global.t('userPage.actionMessages.updateUserAccess'),
            actions: [{ icon: 'close', color: 'white' }],
          });
          void getListUsers();
        }
      } catch (error) {}
    };

    onMounted(() => {
      void getListUsers();
      void getListUserGroup();
    });

    const changePageSize = () => {
      pageIndex.value = 1;
      void getListUsers();
    };
    return {
      userTableRows,
      userTableColumns,
      totalPage,
      pageIndex,
      pageSize,
      keyword,
      // filterListUser,
      showDialog,
      userName,
      password,
      fullName,
      birthday,
      email,
      phoneNumber,
      mobileNumber,
      sex,
      address,
      unit,
      status,
      group,
      scheduleAccess,
      sexOptions,
      listGroup,
      listScheduleAccess,
      id,
      addNewUser,
      getListUserGroup,
      confirmDeleteUser,
      deleteUser,
      confirmResetPassword,
      showDialogUpdate,
      showDialogUpdateUser,
      updateUser,
      getUserDetail,
      openAddUserDialog,
      getListUsers,
      changePageSize,
    };
  },
});
