import { defineComponent, onMounted, Ref, ref } from 'vue';
import { StateInterface, useStore } from 'src/store';
import UserGroupComponent from '../../components/user-group/index.vue';
import GroupInfoComponent from '../../components/group-info/index.vue';
import accountGroup from '../../components/accountGroup/index.vue';
import { AxiosResponse } from 'axios';
import { api, BaseResponseBody } from 'src/boot/axios';
import { EditMode } from 'src/assets/enums';
import { API_PATHS, config } from 'src/assets/configurations.example';
import { Dialog, Notify } from 'quasar';
import { i18n } from 'src/boot/i18n';
import { Store } from 'vuex'

export type GroupInfoType = {
  createBy: null | string;
  createTime: null | string;
  description: string;
  groupName: string;
  id: number;
  status: number;
  updateBy: null | string;
  updateTime: null | string;
};
const tab = ref('inforGroup');
const userList: Ref<GroupInfoType[]> = ref([]);
const isDisable = ref(EditMode.default);
const groupName = ref('');
const groupDescription = ref('');
const selectedGroupId = ref(-1);
const selectedPageRoles: Ref<string[]> = ref([]);

const addNewGroupInfo = async ($store: Store<StateInterface>) => {
  try {
    const response = (await api({
      // url: API_PATHS.addNewGroupUser,
      method: 'POST',
      data: {
        group: {
          groupName: groupName.value.trim(),
          description: groupDescription.value.trim(),
        },
        users: [],
        pageRoles: [],
      },
    })) as AxiosResponse<BaseResponseBody<unknown>>;
    if (response.data.error.code === config.API_RES_CODE.OK.code) {
      Notify.create({
        type: 'positive',
        message: i18n.global.t(
          'userGroupPage.groupInfo.actionMessages.addNewSuccess'
        ),
        actions: [{ icon: 'close', color: 'white' }],
      });

      changeValueIsDisable(EditMode.default);
      void getListUserGroup($store);
    }
  } catch (error) {}
};

const updateGroupInfo = async ($store: Store<StateInterface>) => {
  try {
    const pages = selectedPageRoles.value
      .map((role) => {
        const page = role.split('-')[1];
        return Number(page);
      })
      .reduce((acc: number[], id) => {
        if (!acc.includes(id)) {
          acc.push(id);
        }
        return acc;
      }, []);

    const pageRoles = pages.reduce(
      (acc: { id: number; roles: string }[], page) => {
        const parsedPage = {
          ...$store.state.authentication.pageList.find((p) => p.id === page),
          roles: '',
        };

        parsedPage.roles = selectedPageRoles.value
          .reduce((a: number[], pageRole) => {
            const splittedRoles = pageRole.split('-');

            if (Number(splittedRoles[1]) === page) {
              a.push(Number(splittedRoles[0]));
            }

            return a;
          }, [])
          .join(',');

        acc.push({
          id: parsedPage.id || -1,
          roles: parsedPage.roles,
        });
        return acc;
      },
      []
    );

    const response = (await api({
      url: API_PATHS.updateUserGroupInfo,
      method: 'POST',
      data: {
        group: {
          id: selectedGroupId.value,
          groupName: groupName.value.trim(),
          description: groupDescription.value.trim(),
        },
        pageRoles,
      },
    })) as AxiosResponse<BaseResponseBody<unknown>>;
    if (response.data.error.code === config.API_RES_CODE.OK.code) {
      Notify.create({
        type: 'positive',
        message: i18n.global.t(
          'userGroupPage.groupInfo.actionMessages.updateSuccess'
        ),
        actions: [{ icon: 'close', color: 'white' }],
      });

      changeValueIsDisable(EditMode.default);
      void getListUserGroup($store);
    }
  } catch (error) {}
};

const changeValueIsDisable = (value: number) => {
  isDisable.value = value;
  groupName.value = '';
  groupDescription.value = '';
  selectedGroupId.value = -1;
};

const saveGroupInfo = ($store: Store<StateInterface>) => {
  //...
  if (isDisable.value === EditMode.add) {
    void addNewGroupInfo($store);
  } else {
    void updateGroupInfo($store);
  }
};

const getListUserGroup = async ($store: Store<StateInterface>) => {
  await $store.dispatch('authentication/getListUsers').then((response) => {
    const res = response as AxiosResponse<BaseResponseBody<GroupInfoType[]>>;
    if (!res.data.error.code) {
      userList.value = res.data.data;
    }
  });
};

const confirmDeleteGroup = ($store: Store<StateInterface>) => {
  Dialog.create({
    title: i18n.global.t(
      'userGroupPage.confirmActionsTitle.confirmDeleteGroupTitle'
    ),
    message: i18n.global
      .t('userGroupPage.confirmActionsTitle.confirmDeleteGroupContent')
      .replace('{groupName}', groupName.value),
    cancel: i18n.global.t(
      'userGroupPage.confirmActionsTitle.confirmDeleteGroupCancelBtnLabel'
    ),
    color: 'negative',
  }).onOk(() => {
    void deleteGroup($store);
  });
};

const deleteGroup = async ($store: Store<StateInterface>) => {
  try {
    const deleteResult = (await api({
      url: API_PATHS.deleteGroupUser,
      method: 'GET',
      params: {
        groupId: selectedGroupId.value,
      },
    })) as AxiosResponse<BaseResponseBody<unknown>>;

    if (deleteResult.data.error.code === config.API_RES_CODE.OK.code) {
      Notify.create({
        type: 'positive',
        message: i18n.global.t(
          'userGroupPage.groupInfo.actionMessages.deleteSuccess'
        ),
        actions: [{ icon: 'close', color: 'white' }],
      });

      changeValueIsDisable(EditMode.default);

      void getListUserGroup($store);
    }
  } catch (error) {}
};

const getGroupDetail = async () => {
  try {
    const groupDetailRes = (await api({
      url: API_PATHS.getUserGroupDetail,
      method: 'GET',
      params: {
        groupId: selectedGroupId.value,
      },
    })) as AxiosResponse<BaseResponseBody<unknown>>;

    if (groupDetailRes.data.error.code === config.API_RES_CODE.OK.code) {
      // selectedPageRoles.value
      const groupDetail = groupDetailRes.data.data as {
        pageRoles: {
          id: number;
          roles: string;
        }[];
      };

      selectedPageRoles.value = groupDetail.pageRoles.reduce(
        (acc: string[], pageRole) => {
          const splittedRoles = pageRole.roles.split(',');
          splittedRoles.forEach((role) => {
            acc.push(`${role}-${pageRole.id}`);
          });

          return acc;
        },
        []
      );
    }
  } catch (error) {}
};

export default defineComponent({
  components: { UserGroupComponent, GroupInfoComponent, accountGroup },
  watch: {
    selectedPageRoles() {
      // console.log(value);
    },
  },
  setup() {
    const $store = useStore();
    const getGroupInfo = (groupInfo: GroupInfoType) => {
      isDisable.value = EditMode.edit;
      selectedGroupId.value = groupInfo.id;
      groupName.value = groupInfo.groupName;
      groupDescription.value = groupInfo.description;
      void getGroupDetail();
    };

    onMounted(() => {
      void getListUserGroup($store);
    });
    return {
      userList,
      getGroupInfo,
      isDisable,
      changeValueIsDisable,
      groupName,
      groupDescription,
      selectedGroupId,
      saveGroupInfo,
      confirmDeleteGroup,
      selectedPageRoles,
      tab,
    };
  },
});
