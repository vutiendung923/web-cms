const isEmail = (str: string) => {
  const email = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return email.test(str);
};
const isMobilePhone = (str: string) => {
  const phoneNot84 = /[0]{1}[0-9]{9,11}$/;
  const phone84 = /^[84]{2}[0-9]{9,11}$/;
  return phoneNot84.test(str) || phone84.test(str);
};
export { isEmail, isMobilePhone };
