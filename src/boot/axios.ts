import { boot } from 'quasar/wrappers';
import axios, {
  AxiosError,
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
} from 'axios';
import { config } from 'src/assets/configurations.example';
import { StateInterface } from 'src/store';
// import { Store } from 'vuex';
import { Notify } from 'quasar';
import { i18n } from './i18n';
import { StatusCodes } from 'http-status-codes';
export * from '@vue/runtime-core'
// import { StatusCodes } from 'http-status-codes';

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $axios: AxiosInstance;
  }
}

const api = axios.create({ baseURL: config.API_ENDPOINT });

export type BaseResponseBody<T> = {
  error: {
    code: number;
    message: string;
  };
  data: T;
};

export default boot<StateInterface>(({ app, store }) => {
  // for use inside Vue files (Options API) through this.$axios and this.$api

  const onRequest = (axiosConfig: AxiosRequestConfig) => {
    // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
    axiosConfig.headers.Authorization = `Bearer ${
      store.state.authentication.token || ''
    }`;

    if (axiosConfig.method?.toUpperCase() === 'GET') {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      axiosConfig.params.channel = config.CHANNEL;
    } else if (axiosConfig.method?.toUpperCase() === 'POST') {
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      axiosConfig.data.channel = config.CHANNEL;
    }
    return axiosConfig;
  };

  const onRequestError = (error: Error) => {
    Notify.create({
      type: 'negative',
      message: i18n.global.t('requestErrorMessage'),
    });
    console.log('[REQUEST ERROR]: ', error);
    return Promise.reject(error);
  };

  const onResponse = (res: AxiosResponse<BaseResponseBody<unknown>>) => {
    if (res.data.error.code) {
      if (
        res.data.error.code === config.API_RES_CODE.TOKEN_INVALID.code ||
        res.data.error.code === config.API_RES_CODE.TOKEN_EXPIRES.code
      ) {
        // ...
        Notify.create({
          type: 'warning',
          message: i18n.global.t('tokenInvalidMessage'),
        });
        void store.dispatch('authentication/logOutNotNotification');
        // ... Logout
      } else {
        Notify.create({
          type: 'warning',
          message: res.data.error.message,
        });
      }
    }
    return res;
  };

  const onResponseError = (error: Error | AxiosError) => {
    if (error instanceof Error) {
      Notify.create({
        type: 'negative',
        message: i18n.global.t('responseErrorMessage'),
      });
    } else {
      const axiosError = error as AxiosError;
      const response = axiosError.response as AxiosResponse<
        BaseResponseBody<unknown>
      >;

      if (response.status === StatusCodes.NOT_FOUND) {
        // ...
      }
    }
    console.log('[RESPONSE ERROR]: ', error);
    return Promise.reject(error);
  };

  api.interceptors.request.use(onRequest, onRequestError);
  api.interceptors.response.use(onResponse, onResponseError);

  app.config.globalProperties.$axios = axios;
  // ^ ^ ^ this will allow you to use this.$axios (for Vue Options API form)
  //       so you won't necessarily have to import axios in each vue file

  app.config.globalProperties.$api = api;
  // ^ ^ ^ this will allow you to use this.$api (for Vue Options API form)
  //       so you can easily perform requests against your app's API
});

export { axios, api };
