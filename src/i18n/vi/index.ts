// This is just an example,
// so you can safely delete all default props below

export default {
  failed: 'Action failed',
  success: 'Action was successful',
  requestErrorMessage: 'Không thể kết nối đến server',
  responseErrorMessage: 'Xảy ra lỗi trong quá trình kết nối tới server',
  tokenInvalidMessage: 'Phiên đăng nhập hết hạn. Vui lòng đăng nhập lại',
  loginSuccess: 'Đăng nhập thành công',
  logoutSuccess: 'Đăng xuất thành công',
  loginPage: {
    pageTitle: 'Đăng nhập',
    usernameInputLabel: 'Tên đăng nhập',
    passwordInputLabel: 'Mật khẩu',
    requireUsername: 'Vui lòng Nhập tên đăng nhập',
    requirePassword: 'Vui lòng Nhập mật khẩu',
    submitLoginBtnLabel: 'Đăng nhập',
    loginSuccess: 'Đăng nhập thành công',
  },
  responseErrorMsg: {
    'msg-1': 'Lỗi không xác định',
    msg999: 'Lỗi hệ thống',
    msg99: 'Lỗi hệ thống',
    msg1016: 'Tài khoản hoặc Mật khẩu không đúng',
    msg4: 'Tên đơn vị chủ quản đã tồn tại',
  },
  emptyData: 'Không có dữ liệu',
  crudActions: {
    add: 'Thêm',
    update: 'Cập nhật',
    delete: 'Xoá',
    search: 'Tìm kiếm',
    details: 'Chi tiết',
    back: 'Quay lại',
  },

  userGroupPage: {
    title: 'Nhóm người dùng',
    groupInfo: {
      title: 'Thông tin nhóm',
      accountGroup: 'Tài khoản nhóm',
      validateMessages: {
        requireGroupName: 'Vui lòng nhập Tên nhóm người dùng',
        groupNameLengthInvalid:
          'Tên nhóm người dùng chỉ có thể dài tối đa 10 ký tự',
        requireGroupDescription: 'Vui lòng nhập Mô tả nhóm người dùng',
      },
      actionMessages: {
        addNewSuccess: 'Thêm nhóm người dùng thành công',
        deleteSuccess: 'Xoá nhóm người dùng thành công',
        updateSuccess: 'Cập nhật nhóm người dùng thành công',
      },

      fieldLabels: {
        groupName: 'Tên nhóm người dùng *',
        groupDescription: 'Mô tả *',
        fullName: 'Họ tên',
        user: 'Tên đăng nhập',
        position: 'Chức vụ',
      },
    },

    confirmActionsTitle: {
      confirmDeleteGroupTitle: 'Xác nhận',
      confirmDeleteGroupContent: 'Bạn có chắc chắn muốn xoá {groupName}?',
      confirmDeleteGroupCancelBtnLabel: 'Huỷ',
    },
  },
  userPage: {
    title: 'Quản lý người dùng',
    tableColumns: {
      userName: 'Tên đăng nhập',
      keywordSearch: 'Tên người dùng',
      fullName: 'Họ tên',
      email: 'Email',
      phone: 'Số điện thoại',
      unit: 'Đơn vị',
      status: 'Trạng thái',
      action: 'Chức năng',
    },
    statusLabel: {
      active: 'Đang hoạt động',
      inactive: 'Ngừng hoạt động',
    },
    dialogLabel: {
      title: {
        addUser: 'Thêm người dùng',
        updateUser: 'Cập nhật người dùng',
      },
      fieldLabels: {
        userName: 'Tên đăng nhập *',
        password: 'Mật khẩu *',
        fullName: 'Họ Tên *',
        email: 'Email *',
        mobileNumber: 'Số điện thoại *',
        address: 'Địa chỉ',
        phoneNumber: 'Số máy bàn',
        unit: 'Đơn vị phòng ban *',
        sex: 'Giới tính *',
        birthday: 'Ngày sinh *',
        group: 'Thuộc nhóm *',
        scheduleAccess: 'Lịch truy cập',
        status: 'Trạng thái',
      },
    },
    crudActions: {
      save: 'Lưu',
      cancel: 'Đóng',
    },
    validateMessages: {
      requireUserName: 'Vui lòng nhập Tên đăng nhập',
      requirePassword: 'Vui lòng nhập Mật khẩu',
      requireFullName: 'Vui lòng nhập Họ tên',
      requireBirthday: 'Vui lòng chọn Ngày sinh',
      requireEmail: 'Vui lòng nhập Email',
      isEmail: 'Email không hợp lệ',
      requireMobileNumber: 'Vui lòng nhập Số điện thoại',
      isMobilePhone: 'Số điện thoại không hợp lệ',
      requireAddress: 'Vui lòng nhập Địa chỉ',
      requireUnit: 'Vui lòng nhập Đơn vị phòng ban',
      requireSex: 'Vui lòng chọn Giới tính',
      requiredGroup: 'Vui lòng chọn Nhóm người dùng',
    },
    confirmActionsTitle: {
      confirmDeleteUserTitle: 'Xác nhận',
      confirmDeleteUserCancelBtnLabel: 'Huỷ',
      confirmDeleteUserContent: 'Bạn có chắc muốn xoá tài khoản',
      confirmResetPasswordContent:
        'Bạn có muốn reset về mật khẩu "123@123a" cho người dùng này không',
    },
    actionMessages: {
      addNewUserAccess: 'Thêm tài khoản thành công',
      deleteUserAccess: 'Xoá tài khoản thành công',
      updateUserAccess: 'Cập nhật thông tin tài khoản thành công',
      resetPasswordAccess: 'Reset mật khẩu thành công',
    },
    toolTipMessage: {
      updateUserInfo: 'Cập nhật',
      resetPassword: 'Reset mật khẩu',
      deleteUser: 'Xoá',
      informationUser: 'Thông tin',
    },
  },

  // Đơn vị chủ quản
  managingUnit: {
    title: 'Đơn vị chủ quản',
    tableColumns: {
      code: 'Mã đơn vị',
      name: 'Tên đơn vị',
      representative: 'Người đại diện',
      fields: 'Lĩnh vực',
      email: 'Email',
      phoneNumber: 'SĐT',
      status: 'Trạng thái',
      action: 'Chức năng',
    },
    statusLabel: {
      active: 'Đang hoạt động',
      inactive: 'Ngừng hoạt động',
    },
    dialogLabel: {
      title: {
        add: 'Thêm đơn vị chủ quản',
        update: 'Cập nhật đơn vị chủ quản',
      },
      fieldLabels: {
        code: 'Mã đơn vị *',
        name: 'Tên đơn vị *',
        representative: 'Người đại diện *',
        address: 'Địa chỉ *',
        email: 'Email *',
        phoneNumber: 'Số điện thoại *',
        fields: 'Lĩnh vực *',
        status: 'Trạng thái',
      },
    },
    toolTipMessage: {
      updateInfo: 'Cập nhật',
      information: 'Thông tin',
      delete: 'Xóa',
    },
    crudActions: {
      save: 'Lưu',
      cancel: 'Đóng',
    },
    validateMessages: {
      requireCode: 'Vui lòng nhập Mã đơn vị',
      requireLengthCode: 'Mã đơn vị không vượt quá 20 ký tự',
      requireLengthRepresentative: 'Tên người đại diện không vượt quá 30 ký tự',
      requireName: 'Vui lòng nhập Tên đơn vị',
      requireRepresentative: 'Vui lòng nhập Người đại diện',
      requireFields: 'Vui lòng chọn Lĩnh vực',

      requireEmail: 'Vui lòng nhập Email',
      isEmail: 'Email không hợp lệ',
      requirePhoneNumber: 'Vui lòng nhập Số điện thoại',
      isPhone: 'Số điện thoại không hợp lệ',
      requireAddress: 'Vui lòng nhập Địa chỉ',
    },
    confirmActionsTitle: {
      confirmDeleteManagingUnitsTitle: 'Xác nhận',
      confirmDeleteManagingUnitsCancelBtnLabel: 'Huỷ',
      confirmDeleteManagingUnitsContent:
        'Bạn có chắc muốn xoá Đơn vị chủ quản này?',
    },
    actionMessages: {
      addNewManagingUnitsAccess: 'Thêm Đơn vị chủ quản thành công',
      deleteManagingUnitsAccess: 'Xoá Đơn vị chủ quản thành công',
      updateManagingUnitsAccess:
        'Cập nhật thông tin Đơn vị chủ quản thành công',
    },
  },

  //Add đơn vị chủ quản
  managingUnitAdd: {
    AddmanagingUnit: 'Thêm nghệ sỹ',
    AddmanagingBankAccount: 'Thêm ngân hàng',
    titleAdd: 'Danh sách nghệ sỹ',
    titleUnitBankAccount: 'Danh sách tài khoản ngân hàng thụ hưởng',
    noDataTable: 'Không có dữ liệu',
    tableColumns: {
      sttAdd: 'STT',
      nameAdd: 'Tên nghệ sỹ',
      myedityAdd: 'Nghệ danh',
      fieldsAdd: 'Lĩnh vực',
      timeAdd: 'Thời gian hợp đồng',
      action: 'Chức năng',
      //ds nghệ sỹ
      sttUpdate: 'STT',
      nameUpdate: 'Tên nghệ sỹ',
      myedityUpdate: 'Nghệ danh',
      fieldsUpdate: 'Lĩnh vực',
      timeUpdate: 'Thời gian hợp đồng',
      status: 'Trạng thái',
      //ds bankaccount
      cardCode: 'Số tài khoản',
      userCard: 'Chủ tài khoản',
      numberCard: 'Số ghi trên thẻ',
      bank: 'Ngân hàng',
      cardType: 'Loại thẻ',
      isDefault: 'Mặc định',
    },
    statusLabel: {
      active: 'Còn thời gian',
      inactive: 'Hết hạn hợp đồng',
    },
    dialogLabel: {
      title: {
        add: 'Thêm nghệ sỹ',
        update: 'Cập nhật nghệ sỹ',
        addBankAccount: 'Thêm ngân hàng thụ hưởng',
        updateBankAccount: 'Cập nhật ngân hàng thụ hưởng',
      },
      fieldLabels: {
        //ds nghệ sỹ
        artistName: 'Tên nghệ sỹ *',
        contractTimeTo: 'Thời gian kết thúc hợp đồng *',
        artistField: 'Lĩnh vực *',
        contractTimeFrom: 'Thời gian bắt đầu hợp đồng *',
        status: 'Trạng thái',
        //ds bankaccount
        cardCode: 'Số tài khoản *',
        numberCard: 'Số ghi trên thẻ *',
        bank: 'Ngân hàng *',
        cardType: 'Loại thẻ *',
        userCard: 'Chủ tài khoản *',
      },
    },
    toolTipMessage: {
      updateInfo: 'Cập nhật',
      delete: 'Xóa nghệ sỹ',
      updateBankAccount: 'Cập nhật',
      deleteBankAccount: 'Xóa ngân hàng',
    },
    crudActions: {
      save: 'Lưu',
      cancel: 'Đóng',
    },
    validateMessages: {
      //ds nghệ sỹ
      requireArtistName: 'Vui lòng chọn Nghệ sỹ',
      requireContractTimeTo: 'Vui lòng chọn Thời gian kết thúc hợp đồng',
      requireArtistField: 'Vui lòng chọn Lĩnh vực',
      requireContractTimeFrom: 'Vui lòng chọn Thời gian bắt đầu hợp đồng',
      //ds bankaccount
      requireBank: 'Vui lòng chọn Ngân hàng',
      requireCardType: 'Vui lòng chọn Loại thẻ',
      requireCardCode: 'Vui lòng nhập Số tài khoản',
      requireCardCode1: 'Số tài khoản không hợp lệ',
      requireNumberCard: 'Vui lòng nhập Số ghi trên thẻ',
      requireNumberCard1: 'Số ghi trên thẻ không hợp lệ',
      requireUserCard: 'Vui lòng nhập Chủ tài khoản',
    },
    confirmActionsTitle: {
      confirmDelete: 'Bạn có chắc chắn muốn xóa hợp đồng với nghệ sỹ?',
    },
    actionMessages: {
      unitAddArtistSuccess: 'Thêm hợp đồng nghệ sỹ thành công',
      unitUpdateArtistSuccess: 'Cập nhật hợp đồng nghệ sỹ thành công',
      unitDeleteArtistSuccess: 'Xóa hợp đồng nghệ sỹ thành công',
    },
  },

  // khách hàng
  customer: {
    title: 'Danh sách khách hàng',
    tableColumnsCustomer: {
      stt: 'STT',
      customerCode: 'Mã khách hàng',
      userName: 'Tên đăng nhập',
      fullName: 'Họ tên',
      businessName: 'Tên doanh nghiệp',
      taxCode: 'Mã số thuế',
      email: 'Email',
      ratings: 'Xếp hạng',
      status: 'Trạng thái',
      action: 'Chức năng',
    },
    statusLabel: {
      active: 'Đang hoạt động',
      inactive: 'Ngừng hoạt động',
    },
    dialogLabel: {
      title: {
        addCustomer: 'Thêm khách hàng',
        updateCustomer: 'Cập nhật khách hàng',
        editCustomer: 'Cập nhật thông tin khách hàng',
      },
      fieldLabels: {
        userName: 'Tên đăng nhập *',
        customerName: 'Họ tên *',
        password: 'Mật khẩu *',
        code: 'Mã khách hàng *',
        businessName: 'Tên doanh nghiệp *',
        taxCode: 'Mã số thuế *',
        email: 'Email *',
        ratings: 'Xếp hạng *',
        address: 'Địa chỉ *',
        businessType: 'Loại doanh nghiệp *',
        representative: 'Người đại diện *',
        position: 'Chức vụ *',
        phone: 'Số điện thoại *',
        status: 'Trạng thái',
      },
    },
    toolTipMessage: {
      updateCustomerInfo: 'Cập nhật',
      informationCustomer: 'Thông tin',
      deleteCustomer: 'Xoá',
    },
    crudActions: {
      save: 'Lưu',
      cancel: 'Đóng',
    },
    validateMessages: {
      codeExists: 'Mã khách hàng đã tồn tại',
      userNameExists: 'Tên đăng nhập đã tồn tại',
      taxCodeExists: 'Mã số thuế đã tồn tại',
      emailExists: 'Email đã tồn tại',
      phoneExists: 'Số điện thoại đã tồn tại',
      requireUserName: 'Vui lòng nhập Tên đăng nhập',
      lengthUserName: 'Tên đăng nhập không được quá 30 ký tự',
      requirePassword: 'Vui lòng nhập Mật khẩu',
      lengthPassword: 'Độ dài mật khẩu tối thiểu 8 ký tự',
      requireCode: 'Vui lòng nhập Mã khách hàng',
      lengthCode: 'Mã khách hàng không được quá 50 ký tự',
      lengthCustomerName: 'Họ tên không được quá 50 ký tự',
      lengthRepresentative: 'Họ tên người đại diện không được quá 50 ký tự',
      requireCustomerName: 'Vui lòng nhập Họ tên',
      requireBusinessName: 'Vui lòng nhập Tên Doanh nghiệp',
      requireTaxCode: 'Vui lòng nhập Mã số thuế',
      LengthTaxCode: 'Mã số thuế không được quá 20 ký tự',
      requireEmail: 'Vui lòng nhập Email',
      isEmail: 'Email không hợp lệ',
      requirePhone: 'Vui lòng nhập Số điện thoại',
      isPhone: 'Số điện thoại không hợp lệ',
      requireAddress: 'Vui lòng nhập Địa chỉ',
      requireBusinessType: 'Vui lòng nhập Loại doanh nghiệp',
      requireRatings: 'Vui lòng chọn Xếp hạng',
      requireRepresentative: 'Vui lòng nhập Người đại diện',
      requiredPosition: 'Vui lòng nhập Chức vụ',
    },
    confirmActionsTitle: {
      confirmDeleteUserTitle: 'Xác nhận',
      confirmDeleteUserCancelBtnLabel: 'Huỷ',
      confirmDeleteUserContent: 'Bạn có chắc muốn xoá khách hàng này không ?',
      confirmResetPasswordContent:
        'Bạn có muốn reset mật khẩu của người dùng này không ?',
    },
    actionMessages: {
      addNewUserAccess: 'Thêm khách hàng thành công',
      deleteUserAccess: 'Xoá khách hàng thành công',
      updateUserAccess: 'Cập nhật thông tin khách hàng thành công',
      resetPasswordAccess: 'Reset mật khẩu thành công',
    },
  },

  artist: {
    title: 'Nghệ sỹ',
    tableColumnsArtist: {
      artistCode: 'Mã nghệ sỹ',
      fullName: 'Họ tên',
      artistName: 'Nghệ danh',
      avatar: 'Avatar',
      field: 'Lĩnh vực',
      work: 'Công việc',
      qualification: 'Độ chuyên',
      artistLevel: 'Xếp hạng',
      favoriteScore: 'Số lượt thích',
      action: 'Chức năng',
    },
    dialogLabel: {
      title: {
        addStory: 'Thêm Story',
        updateStory: 'Chỉnh sửa Story',
        addArtist: 'Thêm nghệ sỹ',
        updateArtist: 'Cập nhật nghệ sỹ',
        addAccountBank: 'Thêm tài khoản ngân hàng',
        editAccountBank: 'Cập nhật tài khoản ngân hàng',
        addHotProduct: 'Thêm sản phẩm nổi bật',
        updateHotProduct: 'Cập nhật sản phẩm nổi bật',
      },
      fieldLabels: {
        accountOwner: 'Chủ tài khoản',
        accountNumber: 'Số tài khoản',
        cardNumber: 'Số ghi trên thẻ',
        bankName: 'Ngân hàng',
        cardType: 'Loại thẻ',
        isDefault: 'Mặc định',
      },
      validateMessages: {
        requireAccountOwner: 'Vui lòng nhập Chủ tài khoản',
        requireAccountNumber: 'Vui lòng nhập Số tài khoản',
        requireAccountNumber1: 'Số tài khoản không hợp lệ',
        accountNumberExits: 'Số tài khoản đã tồn tại',
        requireCardNumber: 'Vui lòng nhập Số ghi trên thẻ',
        requireCardNumber1: 'Số ghi trên thẻ không hợp lệ',
        requireBankName: 'Vui lòng chọn Ngân hàng',
        requireCardType: 'Vui lòng chọn Loại thẻ',
        errorIsDefault: 'Nghệ sỹ đã có tài khoản ngân hàng mặc định',
        addAccess: 'Thêm tài khoản ngân hàng thành công',
        editAccess: 'Cập nhật thông tin tài khoản ngân hàng thành công',
        deleteAccess: 'Xoá tài khoản ngân hàng thành công',
        deleteStoryAccess: 'Xoá Story thành công',
        addHotProductSccess: 'Thêm sản phẩm thành công',
        updateHotProductSccess: 'Cập nhật sản phẩm thành công',
        deleteHotProductSccess: 'Xóa sản phẩm thành công',
      },
    },
    confirmActionsTitle: {
      confirmDeleteArtistTitle: 'Xác nhận',
      confirmDeleteArtistContent: 'Bạn có chắc muốn xoá nghệ sỹ này không?',
      confirmDeleteEmbedContent: 'Bạn có chắc muốn xoá Embed này không?',
      confirmDeleteBannerContent: 'Bạn có chắc muốn xoá Ảnh banner này không?',
      confirmDeleteArtistBtnLabel: 'Huỷ',
    },
    actionMessages: {
      deleteArtistAccess: 'Xoá nghệ sỹ thành công',
      editArtistAccess: 'Cập nhật thông tin nghệ sỹ thành công',
      addNewArtistAccess: 'Thêm nghệ sỹ thành công',
      EmbedAddSuccess: 'Thêm link Embed thành công',
      EmbedUpdateSuccess: 'Thay đổi link Embed thành công',
      EmbedDeleteSuccess: 'Xóa link Embed thành công',
      StoryAddSuccess: 'Thêm Story thành công',
      bannerDeleteAccess: 'Xóa banner thành công',
      bannerAddAccess: 'Thêm banner thành công',
      bannerUpdateAccess: 'Cập nhật banner thành công',
    },
    artistInformation: {
      tabLabel: {
        personalInformation: 'Thông tin cá nhân',
        vabAccout: 'Tài khoản VAB',
        bankAcount: 'Tài khoản ngân hàng thụ hưởng',
        hotProduct: 'Sản phẩm nổi bật',
      },

      titleDataField: {
        id: 'ID',
        artistCode: 'Mã nghệ sỹ',
        fullName: 'Họ tên',
        artistName: 'Nghệ danh',
        birthday: 'Ngày sinh',
        sex: 'Giới tính',
        nationality: 'Quốc tịch',
        address: 'Địa chỉ',
        status: 'Trạng thái',
        field: 'Lĩnh vực',
        work: 'Công việc',
        qualification: 'Độ chuyên',
        artistLevel: 'Xếp hạng',
        type:'Thể loại',
        favoriteScore: 'Số lượt thích',
        phoneNumber: 'Số điện thoại',
        email: 'Email',
        facebook: 'FB Page',
        facebookMessage: 'FB Message',
        instagram: 'Instagram',
        whatsapp: 'Whatsapp',
        userAdminister: 'Tên quản lý',
        infoBooking: 'Quản lý Booking',
        phoneNumberAdminister: 'Số điện thoại quản lý',
        emailAdminister: 'Email quản lý',
        phoneNumberBooking: 'Liên hệ số điện thoại booking',
        emailBooking: 'Liên hệ Email booking',
        facebookMessageBooking: 'Truy cập facebook booking',
        instagramBooking: 'Truy cập instagram booking',
        whatsappBooking: 'Truy cập whatsapp booking',
      },
      validateMessages: {
        requireEmailAdminister: 'Vui lòng nhập Email booking',
        requirePhoneNumberAdminister: 'Vui lòng nhập Số điện thoại booking',
        requireProducName: 'Vui lòng nhập Tên sản phẩm',
        requireProducCode: 'Vui lòng nhập Mã sản phẩm',
        requireUrlembed: 'Vui lòng nhập Url Embed',
        requireStatus: 'Vui lòng nhập Loại sản phẩm',
        requireArtistCode: 'Vui lòng nhập Mã nghệ sỹ',
        requireFullName: 'Vui lòng nhập Họ tên nghệ sỹ',
        requireAccount: 'Vui lòng nhập Tên đăng nhập',
        requireArtistName: 'Vui lòng nhập Nghệ danh',
        requireBirthday: 'Vui lòng chọn Ngày sinh',
        requireEmail: 'Vui lòng nhập Email',
        isEmail: 'Email không hợp lệ',
        requireAddress: 'Vui lòng nhập Địa chỉ',
        requirePhoneNumber: 'Vui lòng nhập Số điện thoại',
        isPhone: 'Số điện thoại không hợp lệ',
        requireSex: 'Vui lòng chọn Giới tính',
        requireNationality: 'Vui lòng chọn Quốc tịch',
        requireField: 'Vui lòng chọn Lĩnh vực',
        requiredWork: 'Vui lòng chọn Công việc',
        requireQualification: 'Vui lòng chọn Độ chuyên',
        requireArtistLevel: 'Vui lòng chọn Xếp hạng',
      },
    },
    bankAccount: {
      tableColumnsBank: {
        accountOwner: 'Chủ tài khoản',
        idCard: 'Số tài khoản',
        numberCard: 'Số ghi trên thẻ',
        bankName: 'Ngân hàng',
        cardType: 'Loại thẻ',
        default: 'Mặc định',
        action: 'Chức năng',
      },
      confirmActionsTitle: {
        confirmDeleteAccBankTitle: 'Xác nhận',
        confirmDeleteAccBankContent:
          'Bạn có chắc muốn xoá tài khoản này không?',
        confirmDeleteAccBankBtnLabel: 'Huỷ',
      },
    },
    hotProduct: {
      tableColumnsProduct: {
        name: 'Tên sản phẩm',
        productCode: 'Mã sản phẩm',
        urlEmbed: 'Url embed',
        productImage: 'Ảnh sản phẩm',
        status: 'Loại sản phẩm',
        action: 'Chức năng',
      },
      statusLabel: {
        active: 'Sản phẩm nổi bật',
        inactive: 'Sản phẩm khác',
      },
      confirmActionsTitle: {
        confirmDeleteHotProdcutTitle: 'Xác nhận',
        confirmDeleteHotProdcutContent:
          'Bạn có chắc muốn xoá sản phẩm này không?',
        confirmDeleteHotProdcutBtnLabel: 'Huỷ',
      },
    },
    vabAccount: {
      confirmActionsTitle: {
        confirmDeleteStory: 'Bạn có chắc muốn xoá Story này không?',
      },
    },
  },
  recordPerPage: 'Số bản ghi',
  uploadImage: {
    uploadBanner: 'Tải lên',
    uploadEmbed: 'Tải lên Embed',
    uploadStory: 'Tải lên Story',
    titleEmbedDialog: 'Cập nhật Link Embed',
  },

  //xếp hạng khách hàng
  customerRank: {
    title: 'Hạng điểm khách hàng',
    tableColumnsCustomerRank: {
      stt: 'STT',
      code: 'Mã xếp hạng',
      name: 'Tên xếp hạng',
      level: 'Loại xếp hạng',
      status: 'Trạng thái',
      action: 'Chức năng',
    },
    statusLabel: {
      active: 'Đang hoạt động',
      inactive: 'Ngừng hoạt động',
    },
    dialogLabel: {
      title: {
        addCustomerRank: 'Thêm danh mục xếp hạng khách hàng',
        updateCustomerRank: 'Cập nhật danh mục xếp hạng khách hàng',
      },
      fieldLabels: {
        code: 'Mã xếp hạng *',
        name: 'Tên xếp hạng *',
        level: 'Loại xếp hạng *',
        status: 'Trạng thái',
      },
    },
    toolTipMessage: {
      updateCustomerRankInfo: 'Cập nhật',
      deleteCustomerRank: 'Xoá',
    },
    crudActions: {
      save: 'Lưu',
      cancel: 'Đóng',
    },
    validateMessages: {
      requireCode: 'Vui lòng nhập Mã xếp hạng',
      requireName: 'Vui lòng nhập Tên xếp hạng',
      requireLevel: 'Vui lòng nhập Loại xếp hạng',
    },
    confirmActionsTitle: {
      confirmDeleteCustomerRankTitle: 'Xác nhận',
      confirmDeleteCustomerRankCancelBtnLabel: 'Huỷ',
      confirmDeleteCustomerRankContent:
        'Bạn có chắc chắn muốn xoá danh mục xếp hạng khách hàng này không?',
    },
    actionMessages: {
      addNewCustomerRankAccess: 'Thêm danh mục xếp hạng khách hàng thành công',
      deleteCustomerRankAccess: 'Xoá danh mục xếp hạng khách hàng thành công',
      updateCustomerRankAccess:
        'Cập nhật danh mục xếp hạng khách hàng thành công',
    },
  },

  //lĩnh vực hoạt động
  field: {
    title: 'Lĩnh vực hoạt động',
    tableColumnsField: {
      stt: 'STT',
      name: 'Tên lĩnh vực',
      description: 'Mô tả',
      status: 'Trạng thái',
      action: 'Chức năng',
    },
    statusLabel: {
      active: 'Đang hoạt động',
      inactive: 'Ngừng hoạt động',
    },
    dialogLabel: {
      title: {
        addField: 'Thêm lĩnh vực hoạt động',
        updateField: 'Cập nhật lĩnh vực hoạt động',
      },
      fieldLabels: {
        name: 'Tên lĩnh vực *',
        description: 'Mô tả *',
        status: 'Trạng thái',
      },
    },
    toolTipMessage: {
      updateField: 'Cập nhật',
      deleteField: 'Xoá',
    },
    crudActions: {
      save: 'Lưu',
      cancel: 'Đóng',
    },
    validateMessages: {
      requireName: 'Vui lòng nhập Tên lĩnh vực',
      requireDescription: 'Vui lòng nhập Mô tả',
    },
    confirmActionsTitle: {
      confirmDeleteFieldTitle: 'Xác nhận',
      confirmDeleteFieldCancelBtnLabel: 'Huỷ',
      confirmDeleteFieldContent:
        'Bạn có chắc chắn muốn xoá lĩnh vực hoạt động này không?',
    },
    actionMessages: {
      addNewFieldAccess: 'Thêm lĩnh vực hoạt động thành công',
      deleteFieldAccess: 'Xoá lĩnh vực hoạt động thành công',
      updateFieldAccess: 'Cập nhật lĩnh vực hoạt động thành công',
    },
  },

  //bài viết
  post: {
    title: 'Danh sách bài viết',
    uploadImg: 'Tải lên ảnh bài viết',
    tableColumnsPost: {
      stt: 'STT',
      name: 'Tên bài viết',
      image: 'Ảnh bài viết',
      category: 'Danh mục bài viết',
      createBy: 'Người thêm',
      createTime: 'Thời gian thêm',
      updateBy: 'Người Cập nhật',
      updateTime: 'Thời gian Cập nhật',
      status: 'Trạng thái',
      action: 'Chức năng',
    },
    statusLabel: {
      active: 'Đang hoạt động',
      inactive: 'Ngừng hoạt động',
    },
    dialogLabel: {
      title: {
        addPost: 'Thêm bài viết',
        updatePost: 'Cập nhật bài viết',
      },
      postLabels: {
        name: 'Tên bài viết *',
        image: 'Ảnh bài viết *',
        category: 'Danh mục bài viết *',
        content: 'Nội dung *',
        status: 'Trạng thái',
      },
    },
    toolTipMessage: {
      updatePost: 'Cập nhật',
      deletePost: 'Xoá',
    },
    crudActions: {
      save: 'Lưu',
      cancel: 'Đóng',
    },
    validateMessages: {
      requireName: 'Vui lòng nhập Tên bài viết',
      requireImage: 'Vui lòng chọn Ảnh bài viết',
      requireContent: 'Vui lòng nhập Nội dung',
      requireCategory: 'Vui lòng chọn Danh mục bài viết',
    },
    confirmActionsTitle: {
      confirmDeletePostTitle: 'Xác nhận',
      confirmDeletePostCancelBtnLabel: 'Huỷ',
      confirmDeletePostContent: 'Bạn có chắc chắn muốn xoá bài viết này không?',
    },
    actionMessages: {
      addNewPostAccess: 'Thêm bài viết thành công',
      deletePostAccess: 'Xoá bài viết thành công',
      updatePostAccess: 'Cập nhật bài viết thành công',
    },
  },

  // Danh mục bài viết
  postCategory: {
    title: {
      postCategory: 'Danh sách danh mục bài viết',
      addPost: 'Thêm mới danh mục bài viết',
      updatePost: 'Cập nhật danh mục bài viết',
      listPosts: 'Danh sách bài viết',
      listPostsSelected: 'Danh sách bài viết đã chọn',
      addPostDialog: 'Thêm bài viết',
    },
    tableColumnsPostCategory: {
      stt: 'STT',
      name: 'Tên danh mục',
      image: 'Ảnh bìa',
      title: 'Mô tả ngắn',
      status: 'Trạng thái',
      action: 'Chức năng',
    },
    tableColumnsPost: {
      stt: 'STT',
      name: 'Tên bài viết',
      createBy: 'Người sửa gần nhất',
      updateTime: 'Thời gian Cập nhật gần nhất',
      status: 'Trạng thái',
      action: 'Chức năng',
      imageMini: 'Ảnh thu nhỏ',
    },
    labelInput: {
      namePost: 'Tên danh mục',
      description: 'Mô tả ngắn',
      shortDescription: 'Short description',
      uploadImg: 'Tải lên ảnh bìa',
      nameEnglish: 'Article category name',
    },
    actionMessages: {
      addPostCategoryAccess: 'Thêm danh mục bài viết thành công',
      deletePostCategoryAccess: 'Xoá danh mục bài viết thành công',
      updatePostCategoryAccess: 'Cập nhật danh mục bài viết thành công',
    },
    confirmActionsTitle: {
      confirmDeletePostCategory:
        'Bạn có chắc muốn xoá danh mục bài viết này không?',
    },
  },

  //banner
  banner: {
    title: 'Danh sách banner',
    uploadImg: 'Tải lên ảnh banner',
    tableColumnsBanner: {
      stt: 'STT',
      name: 'Tên banner',
      numIndex: 'Vị trí',
      image: 'Ảnh',
      status: 'Trạng thái',
      action: 'Chức năng',
    },
    statusLabel: {
      active: 'Hiển thị',
      inactive: 'Ẩn',
    },
    dialogLabel: {
      title: {
        addBanner: 'Thêm banner',
        updateBanner: 'Cập nhật banner',
      },
      bannerLabels: {
        name: 'Tên banner *',
        bannerType: 'Loại banner *',
        numIndex: 'Vị trí *',
        image: 'Ảnh',
        urlTarget: 'Path',
        status: 'Trạng thái',
      },
    },
    toolTipMessage: {
      updateBanner: 'Cập nhật',
      deleteBanner: 'Xoá',
    },
    crudActions: {
      save: 'Lưu',
      cancel: 'Đóng',
    },
    validateMessages: {
      requireName: 'Vui lòng nhập Tên',
      requireLocation: 'Vui lòng nhập Vị trí',
    },
    confirmActionsTitle: {
      confirmDeleteBannerTitle: 'Xác nhận',
      confirmDeleteBannerCancelBtnLabel: 'Huỷ',
      confirmDeleteBannerContent: 'Bạn có chắc chắn muốn xoá Banner này không?',
    },
    actionMessages: {
      addNewBannerAccess: 'Thêm banner thành công',
      deleteBannerAccess: 'Xoá banner thành công',
      updateBannerAccess: 'Cập nhật banner thành công',
    },
  },

  //đối tác truyền thông
  configPartner: {
    title: 'Đối tác truyền thông',
    uploadImg: 'Tải lên ảnh',
    tableColumnsConfigPartner: {
      name: 'Tên',
      numIndex: 'Vị trí',
      image: 'Ảnh',
      status: 'Trạng thái',
      action: 'Chức năng',
    },
    statusLabel: {
      active: 'Hiển thị',
      inactive: 'Ẩn',
    },
    dialogLabel: {
      title: {
        addConfigPartner: 'Thêm đối tác truyền thông',
        updateConfigPartner: 'Cập nhật đối tác truyền thông',
      },
      configPartnerLabels: {
        name: 'Tên *',
        numIndex: 'Vị trí *',
        image: 'Ảnh',
        status: 'Trạng thái',
      },
    },
    toolTipMessage: {
      updateConfigPartner: 'Cập nhật',
      deleteConfigPartner: 'Xoá',
    },
    crudActions: {
      save: 'Lưu',
      cancel: 'Đóng',
    },
    validateMessages: {
      requireName: 'Vui lòng nhập Tên',
      requireLocation: 'Vui lòng nhập Vị trí',
    },
    confirmActionsTitle: {
      confirmDeleteConfigPartnerTitle: 'Xác nhận',
      confirmDeleteConfigPartnerCancelBtnLabel: 'Huỷ',
      confirmDeleteConfigPartnerContent:
        'Bạn có chắc chắn muốn xoá Đối tác này không?',
    },
    actionMessages: {
      addNewConfigPartnerAccess: 'Thêm Đối tác thành công',
      deleteConfigPartnerAccess: 'Xoá Đối tác thành công',
      updateConfigPartnerAccess: 'Cập nhật Đối tác thành công',
    },
  },

  //thông tin chung
  infoVAB: {
    title: 'Thông tin chung',
    uploadImg: 'Tải lên ảnh logo',
    titleBankAccount: 'Danh sách tài khoản ngân hàng thụ hưởng',
    tableColumnsInfoVAB: {
      stt: 'STT',
      comName: 'Tên công ty',
      logo: 'Logo',
      website: 'Website',
      phone: 'Số điện thoại',
      email: 'Email',
      address: 'Địa chỉ',
      action: 'Chức năng',
      //ds bankaccount
      cardCode: 'Số tài khoản',
      userCard: 'Chủ tài khoản',
      numberCard: 'Số ghi trên thẻ',
      bank: 'Ngân hàng',
      cardType: 'Loại thẻ',
      isDefault: 'Mặc định',
    },
    dialogLabel: {
      title: {
        updateInfoVAB: 'Cập nhật thông tin',
        //ngân hàng
        addBankAccount: 'Thêm ngân hàng thụ hưởng',
        updateBankAccount: 'Cập nhật ngân hàng thụ hưởng',
      },
      infoVABLabels: {
        comName: 'Tên công ty *',
        website: 'Website *',
        phone: 'Số điện thoại *',
        email: 'Email *',
        address: 'Địa chỉ *',
        facebook: 'Facebook',
        instagram: 'Instagram',
        twitter: 'Twitter',
        snapchat: 'Snapchat',
        linkedin: 'Linkedin',
        //ngân hàng
        cardCode: 'Số tài khoản *',
        numberCard: 'Số ghi trên thẻ *',
        bank: 'Ngân hàng *',
        cardType: 'Loại thẻ *',
        userCard: 'Chủ tài khoản *',
      },
    },
    toolTipMessage: {
      updateInfoVAB: 'Cập nhật',
      deleteInfoVAB: 'Xoá',
      //ngân hàng
      updateBankAccount: 'Cập nhật',
      deleteBankAccount: 'Xóa ngân hàng',
    },
    crudActions: {
      save: 'Lưu',
      cancel: 'Đóng',
    },
    validateMessages: {
      requireName: 'Vui lòng nhập Tên công ty',
      requireWebsite: 'Vui lòng nhập Website',
      requirePhone: 'Vui lòng nhập Số điện thoại',
      requireEmail: 'Vui lòng nhập Email',
      requireAddress: 'Vui lòng nhập Địa chỉ',
      //ngân hàng
      requireBank: 'Vui lòng chọn Ngân hàng',
      requireCardType: 'Vui lòng chọn Loại thẻ',
      requireCardCode: 'Vui lòng nhập Số tài khoản',
      requireNumberCard: 'Vui lòng nhập Số ghi trên thẻ',
      requireUserCard: 'Vui lòng nhập Chủ tài khoản',
    },
    confirmActionsTitle: {
      confirmDeleteInfoVABTitle: 'Xác nhận',
      confirmDeleteInfoVABCancelBtnLabel: 'Huỷ',
      confirmDeleteInfoVABContent:
        'Bạn có chắc chắn muốn xoá Thông tin này không?',
    },
    actionMessages: {
      deleteInfoVABAccess: 'Xoá thông tin thành công',
      updateInfoVABAccess: 'Cập nhật thông tin thành công',
    },
  },

  //booking
  listBooking: {
    toolTipMessage: 'Thông tin chi tiết',
    title: 'Danh sách booking',
    titleColumnsTable: {
      stt: 'STT',
      bookingCode: 'Mã',
      userName: 'Khách hàng',
      artistName: 'Nghệ sỹ',
      content: 'Nội dung',
      address: 'Địa điểm',
      fromTime: 'Thời gian bắt đầu',
      toTime: 'Thời gian kết thúc',
      fee: 'Giá',
      favoriteScore: 'Số lượt thích',
      status: 'T.T Booking',
      performStatus: 'T.T Thực hiện',
      action: 'Chức năng',
    },
    dialogLabel: {
      title: 'Thông tin Booking',
    },
    crudActions: {
      cancel: 'Đóng',
    },
    statusLabel: {
      waitBooking: 'Chờ nhận',
      activeBooking: 'Đã nhận',
      inactiveBooking: 'Từ chối',
      activeEvent: 'Đã diễn',
      inactiveEvent: 'Chưa diễn',
    },
  },
  // công việc
  work: {
    title: 'Danh sách công việc',
    titleColumnsTable: {
      stt: 'STT',
      workName: 'Tên công việc',
      fieldName: 'Lĩnh vực',
      description: 'Mô tả',
      status: 'Trạng thái',
      createTime: 'Thời gian tạo',
      createBy: 'Người tạo',
      updateBy: 'Người Cập nhật',
      updateTime: 'Thời gian Cập nhật',
      action: 'Chức năng',
    },
    statusLabel: {
      active: 'Đang hoạt động',
      inactive: 'Ngừng hoạt động',
    },
  },

  //Cấu hình trang tĩnh
  listConfigSystem: {
    add: 'Thêm',
    toolTipMessageUpdate: 'Cập nhật',
    toolTipMessageDelete: 'Xóa',
    title: 'Cấu hình Trang tĩnh',
    titleColumnsTable: {
      stt: 'STT',
      namePage: 'Tên cấu hình',
      nameMenu: 'Loại menu',
      url: 'Path',
      content: 'Nội dung',
      numIndex: 'Vị trí',
      action: 'Chức năng',
    },
    dialogLabel: {
      titleAdd: 'Thêm Cấu hình',
      titleUpdate: 'Cập nhật Cấu hình',
      fieldLabels: {
        namePage: 'Tên cấu hình *',
        nameMenu: 'Loại menu *',
        url: 'Path *',
        numIndex: 'Vị trí',
        status: 'Trạng thái',
      },
    },
    validateMessages: {
      requireNamePage: 'Vui lòng nhập Tên Cấu hình',
      requireNameMenu: 'Vui lòng chọn Loại menu',
      requireUrl: 'Vui lòng nhập Path',
    },
    crudActions: {
      save: 'Lưu',
      cancel: 'Đóng',
    },
    statusLabel: {
      active: 'Hoạt động',
      inactive: 'Ngừng hoạt động',
    },
    confirmActionsTitle: {
      confirmDelete: 'Xác nhận',
      confirmDeleteContent: 'Bạn có chắc muốn xoá Cấu hình này không?',
      confirmDeleteCancelBtnLabel: 'Huỷ',
    },
    actionMessages: {
      addSuccess: 'Thêm Cấu hình thành công',
      updateSuccess: 'Cập nhật Cấu hình thành công',
      deleteSuccess: 'Xóa Cấu hình thành công',
    },
  },

  //Cấu hình tin tức
  listNews: {
    add: 'Thêm',
    toolTipMessageUpdate: 'Cập nhật',
    toolTipMessageDelete: 'Xóa',
    title: 'Cấu hình Tin tức',
    titleColumnsTable: {
      stt: 'STT',
      image: 'Ảnh tin tức',
      title: 'Tiêu đề',
      content: 'Nội dung',
      location: 'Vị trí',
      urlTarget: 'Url target',
      createTime: 'Người tạo',
      createBy: 'T.G tạo',
      updateTime: 'Người Cập nhật',
      updateBy: 'T.G Cập nhật',
      status: 'Trạng thái',
      action: 'Chức năng',
    },
    dialogLabel: {
      titleUpdate: 'Cập nhật Tin tức',
      titleAdd: 'Thêm Tin tức',
      fieldLabels: {
        avatar: 'Ảnh tin tức',
        urlTarget: 'Url target',
        title: 'Tiêu đề',
        content: 'Nội dung',
        location: 'Vị trí',
        status: 'Trạng thái',
      },
    },
    validateMessages: {
      requireTitle: 'Vui lòng nhập Tiêu đề',
      requireUrlTarget: 'Vui lòng nhập Url target',
    },
    crudActions: {
      save: 'Lưu',
      cancel: 'Đóng',
    },
    statusLabel: {
      active: 'Hoạt động',
      inactive: 'Ngừng hoạt động',
    },
    actionMessages: {
      addSuccess: 'Thêm Tin tức thành công',
      updateSuccess: 'Cập nhật Tin tức thành công',
      deleteSuccess: 'Xóa Tin tức thành công',
    },
  },

  //Cấu hình SP nổi bật
  listHotProduct: {
    add: 'Thêm',
    toolTipMessageUpdate: 'Cập nhật',
    toolTipMessageDelete: 'Xóa',
    title: 'Cấu hình Sản phẩm nổi bật',
    titleColumnsTable: {
      artistName: 'Tên nghệ sỹ',
      name: 'Tên sản phẩm',
      imageUrl: 'Ảnh sản phẩm',
      embeddedUrl: 'Link Embed',
      salientStatus: 'Trạng thái',
      action: 'Chức năng',
    },
    dialogLabel: {
      titleUpdate: 'Cập nhật Sản phẩm nổi bật',
      titleAdd: 'Thêm Sản phẩm nổi bật',
      fieldLabels: {
        artistName: 'Tên nghệ sỹ *',
        name: 'Tên sản phẩm *',
        embeddedUrl: 'Link Embed *',
        salientStatus: 'Trạng thái',
      },
    },
    validateMessages: {
      requireArtistName: 'Vui lòng nhập Tên nghệ sỹ',
      requireName: 'Vui lòng nhập Tên sản phẩm',
      requireEmbeddedUrl: 'Vui lòng nhập Link embed',
    },
    crudActions: {
      save: 'Lưu',
      cancel: 'Đóng',
    },
    statusLabel: {
      active: 'Hiển thị',
      inactive: 'Không hiển thị',
    },
    confirmActionsTitle: {
      confirmDelete: 'Xác nhận',
      confirmDeleteContent: 'Bạn có chắc muốn xoá Sản phẩm nổi bật này không?',
      confirmDeleteCancelBtnLabel: 'Huỷ',
    },
    actionMessages: {
      addSuccess: 'Thêm cấu hính sản phẩm nổi bật thành công',
      updateSuccess: 'Cập nhật cấu hính sản phẩm nổi bật thành công',
      deleteSuccess: 'Xóa cấu hình sản phẩm nổi bật thành công',
    },
  },

  //Cấu hình menu
  listMenu: {
    add: 'Thêm',
    toolTipMessageUpdate: 'Cập nhật',
    toolTipMessageDelete: 'Xóa',
    title: 'Danh sách Menu',
    titleColumnsTable: {
      stt: 'STT',
      name: 'Tên menu',
      // url: 'Url target',
      // numIndex: 'Vị trí',
      // createTime: 'Người tạo',
      // createBy: 'Thời gian tạo',
      // updateTime: 'Người Cập nhật',
      // updateBy: 'Thời gian Cập nhật',
      status: 'Trạng thái',
      action: 'Chức năng',
    },
    dialogLabel: {
      titleUpdate: 'Cập nhật Menu',
      titleAdd: 'Thêm Menu',
      fieldLabels: {
        name: 'Tên menu',
        // url: 'Url target',
        // numIndex: 'Vị trí',
        status: 'Trạng thái',
      },
    },
    validateMessages: {
      requireName: 'Vui lòng nhập Tên menu',
      // requireUrl: 'Vui lòng nhập Url target',
    },
    crudActions: {
      save: 'Lưu',
      cancel: 'Đóng',
    },
    statusLabel: {
      active: 'Hoạt động',
      inactive: 'Ngừng hoạt động',
    },
    confirmActionsTitle: {
      confirmDelete: 'Xác nhận',
      confirmDeleteContent: 'Bạn có chắc muốn xoá Menu này không?',
      confirmDeleteCancelBtnLabel: 'Huỷ',
    },
    actionMessages: {
      addSuccess: 'Thêm Menu thành công',
      updateSuccess: 'Cập nhật Menu thành công',
      deleteSuccess: 'Xóa Menu thành công',
    },
  },
};
