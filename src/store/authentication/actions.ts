import { AxiosResponse } from 'axios';
import { Notify } from 'quasar';
import { API_PATHS } from 'src/assets/configurations.example';
import { api, BaseResponseBody } from 'src/boot/axios';
import { i18n } from 'src/boot/i18n';
import { Router } from 'src/router';
import { Pages } from 'src/router/routes';
import { ActionTree } from 'vuex';
import { StateInterface } from '../index';
import {
  AuthenticationState,
  MenuItem,
  Page,
  PageRole,
  RoleInfo,
} from './state';

const constructMenuItem = (
  menuItem: MenuItem,
  pageRoles: RoleInfo[],
  parentRole: RoleInfo
) => {
  if (!menuItem.pageUrl) {
    menuItem.children = [];

    menuItem.children.push(
      ...pageRoles.filter((r) => r.parentId === parentRole.id)
    );

    menuItem.children.forEach((child) => {
      if (!child.pageUrl) {
        constructMenuItem(child, pageRoles, menuItem);
      }
    });
  }
};

const actions: ActionTree<AuthenticationState, StateInterface> = {
  async callAPILogin(context, payload: { userName: string; password: string }) {
    try {
      const response = (await api({
        url: API_PATHS.login,
        method: 'POST',
        data: payload,
      })) as AxiosResponse<BaseResponseBody<unknown>>;
      if (!response.data.error.code) {
        const res = response as AxiosResponse<
          BaseResponseBody<AuthenticationState>
        >;

        const menuList = res.data.data.pageRoles.reduce(
          (acc: MenuItem[], role) => {
            if (!role.parentId) {
              const menuItem: MenuItem = {
                ...role,
              };

              constructMenuItem(menuItem, res.data.data.pageRoles, role);

              // menuItem.children = [];

              // menuItem.children.push(
              //   ...res.data.data.pageRoles.filter((r) => r.parentId === role.id)
              // );

              acc.push(menuItem);
            }
            return acc;
          },
          []
        );
        context.commit('setMenuList', menuList);
        context.commit('setToken', res.data.data.token);
        context.commit('setUserInfo', res.data.data.user);
        context.commit('setPageInfo', res.data.data.pageRoles);
        context.commit('setGroupInfo', res.data.data.groups);

        Notify.create({
          type: 'positive',
          message: i18n.global.t('loginSuccess'),
        });
        await Router.push({ name: Pages.home });
      }
    } catch (error) {}
  },
  async logOut(context) {
    context.commit('setToken', undefined);
    context.commit('setUserInfo', undefined);
    context.commit('setPageInfo', undefined);
    context.commit('setGroupInfo', undefined);
    Notify.create({
      type: 'positive',
      message: i18n.global.t('logoutSuccess'),
    });
    await Router.push({ name: Pages.login });
  },
  async logOutNotNotification(context) {
    context.commit('setToken', undefined);
    context.commit('setUserInfo', undefined);
    context.commit('setPageInfo', undefined);
    context.commit('setGroupInfo', undefined);
    await Router.push({ name: Pages.login });
  },
  async getListPages(context) {
    try {
      const response = (await api({
        url: API_PATHS.getListPages,
        method: 'GET',
        params: {},
      })) as AxiosResponse<BaseResponseBody<unknown>>;
      if (!response.data.error.code) {
        const res = response as AxiosResponse<
          BaseResponseBody<{ pages: Page[]; roles: PageRole[] | null }>
        >;
        const pageList = res.data.data.pages.reduce((acc: Page[], page) => {
          if (page.id === 1 || page.parentId) {
            page.formatted_role_list = [];
            if (res.data.data.roles) {
              page.formatted_role_list.push(
                ...res.data.data.roles.filter(
                  (page_role) => page_role.pageId === page.id
                )
              );
            }
            acc.push(page);
          }
          return acc;
        }, []);

        context.commit('setPageList', pageList);
      }
    } catch (error) {}
  },
  async getListUsers() {
    try {
      const response = (await api({
        url: API_PATHS.getListGroupUsers,
        method: 'GET',
        params: {},
      })) as AxiosResponse<BaseResponseBody<unknown>>;
      return response;
    } catch (error) {}
  },
  async addNewGroupUser(payload) {
    try {
      const response = (await api({
        url: API_PATHS.addNewGroupUser,
        method: 'POST',
        data: payload,
      })) as AxiosResponse<BaseResponseBody<unknown>>;
      return response;
    } catch (error) {}
  },
};

export default actions;
