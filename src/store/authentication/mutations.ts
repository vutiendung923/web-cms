import { MutationTree } from 'vuex';
import {
  AuthenticationState,
  MenuItem,
  Page,
  RoleInfo,
  UserInfo,
} from './state';

const mutation: MutationTree<AuthenticationState> = {
  setToken(state, payload: string) {
    state.token = payload;
  },
  setUserInfo(
    state,
    payload: {
      email: string;
      fullName: string;
      id: number;
    }
  ) {
    state.user = payload;
  },
  setPageInfo(state, payload: RoleInfo[]) {
    state.pageRoles = payload;
  },
  setGroupInfo(
    state,
    payload: {
      group_id: number;
      group_name: string;
    }[]
  ) {
    state.groups = payload;
  },
  setPageList(state, payload: Page[]) {
    state.pageList = payload;
  },
  setMenuList(state, payload: MenuItem[]) {
    state.menuList = payload;
  },
  setUserList(state, payload: UserInfo[]) {
    state.userList = payload;
  },
};

export default mutation;
