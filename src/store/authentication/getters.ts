import { GetterTree } from 'vuex';
import { StateInterface } from '../index';
import { AuthenticationState } from './state';

const getters: GetterTree<AuthenticationState, StateInterface> = {
  someGetter (/* context */) {
    // your code
  }
};

export default getters;
