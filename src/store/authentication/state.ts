export type RoleInfo = {
  description: null | string;
  id: number;
  level: number;
  menuIndex: number;
  pageIcon: string;
  pageName: string;
  pageUrl: null | string;
  parentId: number;
  roles: string;
};
export type Page = {
  defineKey?: string | null;
  id: number;
  level: number;
  menuIndex: number;
  pageIcon: string;
  pageName: string;
  pageUrl: string;
  parentId: number;
  roleList?: string | null;
  formatted_role_list?: PageRole[];
  name?: string;
  roles?: string;
};

export type PageRole = {
  defineKey?: string | null;
  description: string | null;
  pageId: number;
  roleId: number;
  roleName: string;
  status: number;
  id?: number | string;
  name?: string;
};

export type MenuItem = {
  roles: string;
  level: number;
  id: number;
  pageName: string;
  pageUrl: null | string;
  pageIcon: string;
  description: string | null;
  parentId: number;
  menuIndex: number;
  children?: MenuItem[];
};

export type UserInfo = {
  address: string | null;
  birthday: number | null;
  createBy: string | null;
  createTime: number | null;
  email: string | null;
  fullName: string | null;
  id: number;
  isAdmin: boolean | null;
  mobileNumber: number | null;
  password: string;
  phoneNumber: number | null;
  sex: number;
  status: number;
  unit: string | null;
  updateBy: string | null;
  updateTime: number | null;
  userName: string;
};
export interface AuthenticationState {
  token?: string;
  user?: {
    email: string;
    fullName: string;
    id: number;
  };
  pageRoles: RoleInfo[];
  groups?: {
    group_id: number;
    group_name: string;
  }[];
  pageList: Page[];
  menuList: MenuItem[];
  userList: UserInfo[];
}

function state(): AuthenticationState {
  return {
    token: '',
    pageList: [],
    pageRoles: [],
    menuList: [],
    userList: [],
  };
}

export default state;
