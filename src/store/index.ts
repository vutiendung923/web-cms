import { store } from 'quasar/wrappers';
import { InjectionKey } from 'vue';
import {
  createStore,
  Store as VuexStore,
  useStore as vuexUseStore,
} from 'vuex';

import authentication from './authentication';
import { AuthenticationState } from './authentication/state';
import SecureLS from 'secure-ls'
import createPersistedState from 'vuex-persistedstate';
export * from '@vue/runtime-core'
// eslint-disable-next-line
const ls = new SecureLS({ isCompression: true });

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export interface StateInterface {
  // Define your own store structure, using submodules if needed
  // example: ExampleStateInterface;
  // Declared as unknown to avoid linting issue. Best to strongly type as per the line above.
  authentication: AuthenticationState;
}

// provide typings for `this.$store`
declare module '@vue/runtime-core' {
  export interface ComponentCustomProperties {
    $store: VuexStore<StateInterface>;
  }
}

// provide typings for `useStore` helper
export const storeKey: InjectionKey<VuexStore<StateInterface>> = Symbol(
  'vuex-key'
);

export default store(function (/* { ssrContext } */) {
  const Store = createStore<StateInterface>({
    modules: {
      // example
      authentication,
    },

    plugins: [
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call
      createPersistedState({
        paths: ['authentication'],
        storage: {
          // eslint-disable-next-line
          getItem: (key: string) => ls.get(key),
          // eslint-disable-next-line
          setItem: (key: string, value: unknown) => ls.set(key, value),
          // eslint-disable-next-line
          removeItem: (key: string) => ls.remove(key),
        },
      }),
    ],

    // enable strict mode (adds overhead!)
    // for dev mode and --debug builds only
    strict: !!process.env.DEBUGGING,
  });

  return Store;
});

export function useStore() {
  return vuexUseStore(storeKey);
}
