export enum EditMode {
  default = 0,
  add = 1,
  edit = 2,
}

export enum UnitStatus {
  active = 1,
  inactive = 2,
}

export enum HotProductStatus {
  active = 2,
  inactive = 1,
}

export enum CustomerRankStatus {
  active = 1,
  inactive = 0,
}

export enum FieldStatus {
  active = 1,
  inactive = 0,
}

export enum PostStatus {
  active = 1,
  inactive = 0,
}

export enum BannerStatus {
  active = 1,
  inactive = 0,
}

export enum BookingStatus {
  active = 1,
  inactive = 2,
  waitBooking = 0,
}

export enum ActiveStatus {
  active = 1,
  inactive = 0,
}

export enum ContactStatus {
  active = 1,
  inactive = 0,
}

export enum NewsStatus {
  active = 1,
  inactive = 0,
}

export enum SystemHotProductStatus {
  active = 1,
  inactive = 0,
}

export enum MenuStatus {
  active = 1,
  inactive = 0,
}
