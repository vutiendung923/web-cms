import { type } from 'os';

export type PaginationResponse<DataType> = {
  pageIndex: null | number;
  pageSize: null | number;
  totalPages: number;
  totalRecords: number;
  beginIndex: number;
  endIndex: number;
  data: DataType[];
};
export type FileUploadType = {
  byteSize: number;
  createTime: string;
  fileName: string;
  filePath: string;
  folder: string;
  id: number;
  isDeleted: number;
};
export type UserObject = {
  id: number;
  userName: string;
  password: string;
  fullName: null | string;
  birthday: null | string;
  email: null | string;
  phoneNumber: null | string;
  mobileNumber: null | string;
  sex: number;
  address: null | string;
  unit: null | string;
  status: number;
  isAdmin: null | number;
  createTime: null | string;
  createBy: null | string;
  updateTime: null | string;
  updateBy: null | string;
};

export type ArtistInfoType = {
  id: number;
  account: string;
  avatar: string | null;
  artistCode: string;
  fullName: string;
  artistName: string | null;
  birthday: string | null;
  sex: number;
  address: string | null;
  status: number;
  phoneNumber: string | null;
  email: string | null;
  facebook: string | null;
  facebookMessage: string | null;
  instagram: string | null;
  whatsapp: string | null;
  nationality: NationalityType;
  rovince: ProvinceType[];
  fields: FieldType[];
  works: WorkType[];
  qualification: QualificationType;
  artistLevel: ArtistLevelType;
  socialEmbedded: string | null;
  bankAccounts: BankAccountType[];
  banners: BannerType[];
  products: ProductType[];
  schedules: SchedulesType[];
  musicTypeDto:MusicType[];
  provinceDto: ProvinceType[];
  stories: StoriesType[];
  mnName: string | null;
  mnPhone: string | null;
  mnEmail: string | null;
  mnBookingEmail: string | null;
  mnBookingPhone: string | null;
  mnFbmess: string | null;
  mnIns: string | null;
  mnWhatsapp: string | null;
  favoriteScore: number;
};
 export type ProvinceType = {
    name: string,
    fullname: string,
    code: string,
    level: string,
 };

 export type MusicType = {
     id:number,
     name: string,
     status: number,
     code: string,
     numIndex: number,
 };
export type FieldType = {
  id: number;
  name: string;
  status: number;
  description: string | null;
  numIndex: number;
};

export type NationalityType = {
  id: number;
  name: string;
  status: number;
  numIndex: number;
};
export type ArtistLevelType = {
  id: number;
  name: string;
  status: number;
  artistLevel: number;
  description: string | null;
};
export type QualificationType = {
  id: number;
  name: string;
  status: number;
  numIndex: number;
};
export type CardBankType = {
  id: number;
  name: string;
  status: number;
  code: string;
};
export type TypeCardType = {
  id: number;
  name: string;
};
export type WorkType = {
  id: number;
  name: string;
  status: number;
  numIndex: number;
};

export type BannerType = {
  bannerUrl: string;
  id?: number;
  numIndex?: number;
  status?: number;
  url?: string;
  file?: File;
};

export type SchedulesType = {
  id: number;
  artistId: number;
  bookingId: number;
  scheduleTime: string;
  description: string;
  status: number;
};

export type StoriesType = {
  id: number;
  artistId: number;
  title: string;
  content: string;
  imageUrl: string;
  status: number;
  url?: string;
  file?: File;
};

export type BankAccountType = {
  accountNumber: string;
  cardNumber: string;
  bank: CardBankType;
  cardType: TypeCardType;
  isDefault: number;
  id: number;
  nameInCard: string | null;
  status: number;
  accountOwner: string | null;
  branchName: string | null;
};

export type ProductType = {
  code: string;
  embeddedUrl: string;
  id: number;
  imageUrl: string;
  name: string;
  numIndex: number;
  status: number;
  url?: string;
  file?: File;
};

export type ArtistOwner = {
  id: number;
  code: string;
  name: string;
  representative: string;
  fields: string;
  email: string;
  phoneNumber: number;
  status: number;
};

export type ContractType = {
  id: number;
  contractFrom: string;
  contractTo: string;
  status: number;
  artistId: number;
  artistName: string;
  artistFullName: string;
  field: string;
};

export type ArtistDicitionary = {
  id: number;
  name: string;
  qualification: string;
  artistLevel: string;
  field: number;
};

export type Contract = {
  contractFrom: string;
  contractTo: string;
  status: number;
  artistId: number;
  artistName: string;
  artistFullName: string;
  field: string;
  fieldId: number;
};

export type ArtistOwnerAdd = {
  code: string | undefined;
  name: string | undefined;
  representative: string | undefined;
  address: string | undefined;
  email: string | undefined;
  phoneNumber: string | undefined;
  status: number;
  fields: Array<FieldType>;
  contracts: Array<Contract>;
};

export type DetailUnit = {
  id: number;
  name: string;
  code: string;
  representative: string;
  address: string;
  email: string;
  phoneNumber: string;
  status: number;
  fields: Array<FieldType>;
  contracts: Array<Contract>;
};

export type CustomerType = {
  id: number;
  code: string | null;
  userName: string;
  fullName: string;
  companyName: string;
  taxCode: string;
  email: string;
  status: number;
  phone: string;
  password: string;
  address: string;
  type: string;
  representative: string;
  position: string;
  level: CustomerLevelType;
};
export type CustomerLevelType = {
  id: number;
  name: string;
  description: string;
  level: number;
  status: number;
};

export type CustomerRank = {
  id: number;
  code: string;
  name: string;
  description: string;
  level: number;
  status: number;
};

export type AddCustomerRank = {
  code: string;
  name: string;
  description: string;
  level: number;
  status: number;
};

export type UpdateCustomerRank = {
  id: number;
  code: string;
  name: string;
  description: string;
  level: number;
  status: number;
};

export type DetailCustomerRank = {
  id: number;
  code: string;
  name: string;
  description: string;
  level: number;
  status: number;
};

export type Field = {
  id: number;
  name: string;
  description: string;
  status: number;
};

export type AddField = {
  name: string;
  description: string;
  status: number;
};

export type UpdateField = {
  id: number;
  name: string;
  description: string;
  status: number;
};

export type DetailField = {
  id: number;
  name: string;
  description: string;
  status: number;
};

export type PostType = {
  id: number;
  name: string;
  image: string;
  status: number;
  category: string;
  createBy?: string;
  createTime?: string;
  updateBy?: string;
  updateTime?: string;
};

export type LanguageType = {
  id: number;
  code: string;
  name: string;
  status: number;
  isDefault?: number;
};

export type LangType = {
  id: number;
  name: string;
  title: string;
  content: string;
  status: number;
  language: LanguageType;
};

export type CategoryPostType = {
  id: number;
  image: string;
  status: number;
  name: string;
  title: string;
};

export type PostDetailType = {
  id: number;
  image: string;
  status: number;
  langs: LangType[];
  category: CategoryPostType;
  createBy?: string;
  createTime?: string;
  updateBy?: string;
  updateTime?: string;
};

export type PostCategoryDetailType = {
  id: number;
  image: string;
  status: number;
  langs: LangType[];
  posts: PostType[];
  createBy?: string;
  createTime?: string;
  updateBy?: string;
  updateTime?: string;
};

export type PostAddType = {
  id: number;
  image: string;
  status: number;
  langs: LangType[];
  category: CategoryPostType;
  createBy?: string;
  createTime?: string;
  updateBy?: string;
  updateTime?: string;
};

export type MenuType = {
  name: string;
  location: string;
  sites: string;
  createBy: string | null;
  createTime: string | null;
  updateBy: string | null;
  updateTime: string | null;
};

export type ListBooking = {
  id: number;
  bookingCode: number;
  artistName: string;
  userName: string;
  content: string;
  address: string;
  fromTime: string;
  toTime: string;
  status: number;
  favoriteScore: number;
  performStatus: number;
  fee: string;
};

export type ListArrayArtist = {
  id: number;
  artistName: string;
};

export type ListArrayCust = {
  id: number;
  fullName: string;
};

export type listMenu = {
  id: number;
  name: string;
  url: string;
  language: string;
  numIndex: number;
  status: number;
  createBy?: string;
  createTime?: string;
  updateBy?: string;
  updateTime?: string;
};

export type DetailMenu = {
  id: number;
  name: string;
  url: string;
  language: string;
  numIndex: number;
  status: number;
  createBy?: string;
  createTime?: string;
  updateBy?: string;
  updateTime?: string;
};

export type UpdateMenu = {
  id: number;
  name: string;
  url: string;
  numIndex: number;
  status: number;
  createBy?: string;
  createTime?: string;
  updateBy?: string;
  updateTime?: string;
};

export type AddMenu = {
  name: string;
  url: string;
  numIndex: number;
  status: number;
};

export type ListConfigSystem = {
  id: number;
  namePage: string;
  content: string;
  numIndex: number;
  url: string;
  createTime?: string;
  createBy?: string;
  updateTime?: string;
  updateBy?: string;
};

export type DetailConfigSystem = {
  id: number;
  namePage: string;
  content: string;
  numIndex: number;
  url: string;
  createTime?: string;
  createBy?: string;
  updateTime?: string;
  updateBy?: string;
};

export type UpdateConfigSystem = {
  id: number;
  namePage: string;
  content: string;
  url: string;
  numIndex: number;
  createTime?: string;
  createBy?: string;
  updateTime?: string;
  updateBy?: string;
};

export type AddConfigSystem = {
  namePage: string;
  content: string;
  url: string;
  numIndex: number;
};

export type VABInfoType = {
  id: number;
  comName: string;
  logo: string;
  email: string;
  website: string;
  phone: string;
  address: string;
  insLink: string;
  linked: string;
  snapchat: string;
  twtLink: string;
  fbLink: string;
};

export type DetailVABInfoType = {
  id: number;
  comName: string;
  logo: string;
  email: string;
  website: string;
  phone: string;
  address: string;
  insLink: string;
  linked: string;
  snapchat: string;
  twtLink: string;
  fbLink: string;
};

export type UpdateVABInfoType = {
  id: number;
  comName: string;
  logo: string;
  email: string;
  website: string;
  phone: string;
  address: string;
  insLink: string;
  linked: string;
  snapchat: string;
  twtLink: string;
  fbLink: string;
};

export type ListBannerConfig = {
  id: number;
  name: string;
  bannerType: string;
  numIndex: number;
  image: string;
  urlTarget: string;
  status: number;
  createBy: string;
  createTime: string;
  updateBy: string;
  updateTime: string;
};

export type DetailBannerConfig = {
  id: number;
  name: string;
  bannerType: string;
  numIndex: number;
  image: string;
  urlTarget: string;
  status: number;
  createBy: string;
  createTime: string;
  updateBy: string;
  updateTime: string;
};

export type UpdateBannerConfig = {
  id: number;
  name: string;
  bannerType: string;
  numIndex: number;
  image: string;
  urlTarget: string;
  status: number;
  createBy: string;
  createTime: string;
  updateBy: string;
  updateTime: string;
};

export type AddBannerConfig = {
  name: string;
  bannerType: string;
  numIndex: number;
  image: string;
  urlTarget: string;
  status: number;
  createBy: string;
  createTime: string;
  updateBy: string;
  updateTime: string;
};

export type ListConfigPartner = {
  id: number;
  name: string;
  image: string;
  numIndex: number;
  status: number;
  createBy: string;
  createTime: string;
  updateBy: string;
  updateTime: string;
};

export type DetailConfigPartner = {
  id: number;
  name: string;
  image: string;
  numIndex: number;
  status: number;
  createBy: string;
  createTime: string;
  updateBy: string;
  updateTime: string;
};

export type updateConfigPartnerConfig = {
  id: number;
  name: string;
  image: string;
  numIndex: number;
  status: number;
  createBy: string;
  createTime: string;
  updateBy: string;
  updateTime: string;
};

export type addConfigPartnerConfig = {
  name: string;
  image: string;
  numIndex: number;
  status: number;
  createBy: string;
  createTime: string;
  updateBy: string;
  updateTime: string;
};

export type ListConfigHotProduct = {
  id: number;
  code: string;
  name: string;
  artistName: string;
  embeddedUrl: string;
  imageUrl: string;
  salientStatus: number;
};
export type DetailConfigHotProduct = {
  id: number;
  code: string;
  name: string;
  artistName: string;
  embeddedUrl: string;
  imageUrl: string;
  salientStatus: number;
};
export type UpdateConfigHotProduct = {
  id: number;
  code: string;
  name: string;
  artistName: string;
  embeddedUrl: string;
  imageUrl: string;
  salientStatus: number;
};

export type AddConfigHotProduct = {
  code: string;
  name: string;
  artistName: string;
  embeddedUrl: string;
  imageUrl: string;
  salientStatus: number;
};
