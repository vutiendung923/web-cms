export const config = {
  API_ENDPOINT: 'http://cms.vab.xteldev.com/api/',
  API_RES_CODE: {
    OK: {
      code: 0,
    },
    TOKEN_INVALID: {
      code: 1,
    },
    TOKEN_EXPIRES: {
      code: 2,
    },
    '-1': {
      code: -1,
    },
  },
  CHANNEL: 'CMS',
};

export enum API_PATHS {
  login = '/user/login',
  getUserGroups = '/group/get_list',
  getListPages = '/user/page/list',
  getListGroupUsers = '/user/group/list',
  addNewGroupUser = 'user/group/add',
  deleteGroupUser = '/user/group/delete',
  getUserGroupDetail = '/user/group/detail',
  updateUserGroupInfo = '/user/group/update',
  getListUsers = '/user/list',
  addNewUser = '/user/add',
  deleteUser = '/user/delete',
  resetPassword = '/user/resetPass',
  getUserDetail = '/user/detail',
  updateUser = '/user/update',
  getListUnits = '/artistOwner',
  getListArtists = '/artist',
  getFieldOptions = '/field',
  getNationalityOptions = '/nationality',
  getArtistLevelOptions = '/artistLevel',
  getQualificationOptions = '/qualification',
  getWorkOptions = '/work',
}


