import { GroupInfoType } from 'src/pages/nhom-nguoi-dung/UserGroup';
import { computed, defineComponent, PropType, ref } from 'vue';
import { EditMode } from 'src/assets/enums';
// import { useStore } from 'src/store';

export default defineComponent({
  props: {
    listUsers: {
      type: Array as PropType<GroupInfoType[]>,
      required: true,
    },
    selectedGroupId: {
      type: Number,
      required: true,
    },
  },
  emits: ['click:groupName', 'click:addNewGroupUsers'],
  setup(props) {
    // const $store = useStore();
    const keyword = ref('');

    const filteredListUsers = computed(() => {
      return props.listUsers.filter(
        (group) =>
          group.groupName.includes(keyword.value.trim()) ||
          group.groupName.includes(keyword.value.toLowerCase().trim()) ||
          group.groupName.includes(keyword.value.trim().toUpperCase())
      );
    });

    return { keyword, filteredListUsers, EditMode };
  },
});
