import { MenuItem } from 'src/store/authentication/state';
import { computed, defineComponent, PropType } from 'vue';
import { useRoute } from 'vue-router';
export const MenuItemScript = defineComponent({
  name: 'MenuItemComponent',
  props: {
    item: {
      type: Object as PropType<MenuItem>,
      required: true,
    },
  },
  setup(props) {
    const $route = useRoute();
    const isActive = computed(() => {
      return (
        props.item.children &&
        props.item.children?.findIndex((item) => item.pageUrl === $route.path) >
          -1
      );
    });

    return { isActive };
  },
});
