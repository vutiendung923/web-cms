import { defineComponent } from 'vue';
import MenuItemComponent from './menu-item/index.vue';

export const MenuListScript = defineComponent({
  components: {
    MenuItemComponent,
  },
});
