import { defineComponent, ref } from 'vue';
export default defineComponent({
  // name: 'ComponentName'
  props: {
    isBtn: {
      type: Boolean,
      default: false,
    },
    isButton: {
      type: Boolean,
      default: true,
    },
    tooltipMessage: {
      type: String,
      default: '',
    },
    tooltipMessageStory: {
      type: String,
      default: '',
    },
  },
  setup() {
    const upload = ref(null);
    const uploadBanner = () => {
      // eslint-disable-next-line
      // @ts-ignore
      // eslint-disable-next-line
      upload.value?.click();
    };
    return { uploadBanner, upload };
  },
});
