import { i18n } from 'src/boot/i18n';
import { defineComponent, PropType, Ref, ref, watch } from 'vue';
import UploadImage from 'components/upload-image/index.vue';
import {
  PostType,
  LanguageType,
  FileUploadType,
  PostCategoryDetailType,
} from 'src/assets/type';
import ListPostDialog from 'components/post-category/list-post-dialog/index.vue';
import { api, BaseResponseBody } from 'src/boot/axios';
import { AxiosResponse } from 'axios';
import { config } from 'src/assets/configurations.example';

export default defineComponent({
  // name: 'ComponentName'
  components: { UploadImage, ListPostDialog },
  props: {
    isOpenUpdateDialog: { type: Boolean, required: true },
    languages: { type: Array as PropType<LanguageType[]>, required: true },
    detailData: {
      type: Object as PropType<PostCategoryDetailType>,
      required: true,
    },
  },
  setup(props, context) {
    type FromDataType = {
      name: string;
      title: string;
      status: number;
      language: LanguageType;
    };
    watch(
      () => props.isOpenUpdateDialog,
      (value) => {
        if (value) {
          id.value = props.detailData.id;
          imageAPI.value = config.API_IMAGE_ENDPOINT + props.detailData.image;
          imageNotChange.value = props.detailData.image;
          status.value = props.detailData.status;
          langs.value = props.detailData.langs;
          userTableRowsPost.value = props.detailData.posts;
        }
      }
    );
    const id: Ref<number | null> = ref(null);
    const name: Ref<string | null> = ref(null);
    const title: Ref<string | null> = ref(null);
    const image: Ref<string | null> = ref(null);
    const imageAPI: Ref<string | null> = ref(null);
    const imageNotChange: Ref<string | null> = ref(null);
    const status: Ref<number> = ref(1);
    const nameRules = [
      (val?: string) =>
        (val && val.trim().length) ||
        i18n.global.t('post.validateMessages.requireName'),
    ];
    const file: Ref<File | string> = ref('');
    const urlFileLocal: Ref<string> = ref('');
    const keywordSearch: Ref<string | null> = ref(null);
    const userTableColumnsPost = [
      {
        name: 'stt',
        field: 'stt',
        required: true,
        label: i18n.global.t('postCategory.tableColumnsPost.stt'),
        align: 'center',
        sortable: false,
      },
      {
        name: 'name',
        field: 'name',
        required: true,
        label: i18n.global.t('postCategory.tableColumnsPost.name'),
        align: 'center',
        headerStyle: 'text-align: center !important;',
        sortable: false,
      },
      {
        name: 'createBy',
        field: 'createBy',
        required: true,
        label: i18n.global.t('postCategory.tableColumnsPost.createBy'),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'updateTime',
        field: 'updateTime',
        required: true,
        label: i18n.global.t('postCategory.tableColumnsPost.updateTime'),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'status',
        field: 'status',
        required: true,
        label: i18n.global.t('postCategory.tableColumnsPost.status'),
        align: 'center',
        sortable: false,
      },
      // {
      //   name: 'action',
      //   field: 'action',
      //   required: true,
      //   label: i18n.global.t('postCategory.tableColumnsPost.action'),
      //   align: 'center',
      //   sortable: false,
      // },
    ];
    const userTableRowsPost: Ref<PostType[]> = ref([]);
    const isOpenListPost: Ref<boolean> = ref(false);
    const langs: Ref<FromDataType[]> = ref([]);
    const tabName: Ref<string> = ref('vi');
    const tabTitle: Ref<string> = ref('vi');
    const uploadImage = (value: FileList) => {
      urlFileLocal.value = URL.createObjectURL(value[0]);
      file.value = value[0];
      image.value = urlFileLocal.value;
    };
    const imageUploaded: Ref<string | null> = ref(null);
    const deletePostSelected = (postIdx: number) => {
      userTableRowsPost.value.splice(postIdx, 1);
    };
    const callApiUploadAvatar = async (file: File) => {
      try {
        const bodyFormData = new FormData();
        bodyFormData.append('file', file);
        const response = (await api({
          headers: { 'Content-Type': 'multipart/form-data' },
          url: config.API_IMAGE_ENDPOINT,
          method: 'POST',
          data: bodyFormData,
        })) as AxiosResponse<BaseResponseBody<FileUploadType>>;
        if (response.data.error.code === config.API_RES_CODE.OK.code) {
          imageUploaded.value = response.data.data.fileName;
        }
      } catch (error) {}
    };
    const confirmUpdatePostCategory = async () => {
      await callApiUploadAvatar(file.value as File);
      context.emit('updatePostCategory', {
        id: id.value,
        image: image.value ? imageUploaded.value : imageNotChange.value,
        status: status.value,
        posts: userTableRowsPost.value,
        langs: langs.value,
      });
    };

    return {
      name,
      title,
      image,
      status,
      nameRules,
      file,
      urlFileLocal,
      isOpenListPost,
      uploadImage,
      userTableColumnsPost,
      userTableRowsPost,
      keywordSearch,
      deletePostSelected,
      langs,
      tabName,
      tabTitle,
      confirmUpdatePostCategory,
      imageAPI,
    };
  },
  emits: [
    'update:isOpenUpdateDialog',
    'click:closeBtnDialog',
    'SetImage',
    'deleteImage',
    'updatePostCategory',
  ],
});
