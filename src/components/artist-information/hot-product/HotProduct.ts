import { i18n } from 'src/boot/i18n';
import { defineComponent, Ref, ref } from 'vue';
import Pagination from 'components/pagination/index.vue';
import { ProductType } from 'src/assets/type';
import { HotProductStatus } from 'src/assets/enums';
import { config } from 'src/assets/configurations.example';
export default defineComponent({
  components: {
    Pagination,
  },
  props: {
    products: { type: Array, required: true },
    // DataInsertHotProduct: { type: Object, requied: false }
  },
  setup(_, context) {
    const configImg = config;
    const userTableColumnsHotProduct = [
      {
        name: 'name',
        field: 'name',
        required: true,
        label: i18n.global.t('artist.hotProduct.tableColumnsProduct.name'),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'code',
        field: 'code',
        required: true,
        label: i18n.global.t(
          'artist.hotProduct.tableColumnsProduct.productCode'
        ),
        align: 'center',
        sortable: false,
      },
      {
        name: 'embeddedUrl',
        field: 'embeddedUrl',
        required: true,
        label: i18n.global.t('artist.hotProduct.tableColumnsProduct.urlEmbed'),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'imageUrl',
        field: 'imageUrl',
        required: true,
        label: i18n.global.t(
          'artist.hotProduct.tableColumnsProduct.productImage'
        ),
        align: 'center',
        sortable: false,
      },
      {
        name: 'status',
        field: 'status',
        required: true,
        label: i18n.global.t('artist.hotProduct.tableColumnsProduct.status'),
        align: 'center',
        sortable: false,
      },
      {
        name: 'action',
        field: 'action',
        required: true,
        label: i18n.global.t('artist.bankAccount.tableColumnsBank.action'),
        align: 'center',
        sortable: false,
      },
    ];

    const userTableRowsHotProduct: Ref<unknown[]> = ref([]);
    const pageIndex = ref(1);
    const pageSize = ref(20);
    const totalPage = ref(10);
    const updateProduct = (item: { row: ProductType }) => {
      context.emit('setDataUpdate', {
        code: item.row.code,
        embeddedUrl: item.row.embeddedUrl,
        id: item.row.id,
        imageUrl: item.row.imageUrl,
        name: item.row.name,
        status: item.row.status,
        file: item.row.file,
      });
      context.emit('click:openUpdateHotProduct');
    };
    const clickAdd = () => {
      context.emit('click:addHotProduct');
    };
    const deleteRow = (index: number) => {
      context.emit('deleteRow', index);
    };

    return {
      configImg,
      userTableColumnsHotProduct,
      userTableRowsHotProduct,
      pageIndex,
      pageSize,
      totalPage,
      deleteRow,
      clickAdd,
      updateProduct,
      HotProductStatus,
    };
  },
  emits: [
    'click:addHotProduct',
    'reset',
    'deleteRow',
    'click:openUpdateHotProduct',
    'setDataUpdate',
  ],
});
