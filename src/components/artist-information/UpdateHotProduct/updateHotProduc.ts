import { defineComponent, PropType, Ref, ref, watch } from 'vue';
import { i18n } from 'src/boot/i18n';
import UploadImage from '../../upload-image/index.vue';
import { ProductType } from 'src/assets/type';
import { config } from 'src/assets/configurations.example';
import { Notify } from 'quasar';
export default defineComponent({
  components: {
    UploadImage,
  },

  props: {
    openUpdateHotProduct: {
      type: Boolean,
      requied: true,
    },
    dataUpdate: {
      type: Object as PropType<ProductType>,
      requied: false,
    },
  },

  setup(props, context) {
    const configImg = config;
    const file: Ref<File | string> = ref('');
    const name: Ref<string> = ref('');
    const code: Ref<string> = ref('');
    const embeddedUrl: Ref<string> = ref('');
    const urlFileLocal: Ref<string> = ref('');
    const imageAPI: Ref<string | null> = ref(null);
    const status: Ref<number> = ref(2);
    const statusOptions = ref([
      { id: 1, name: 'Sản phẩm khác' },
      { id: 2, name: 'Sản phẩm nổi bật' },
    ]);
    const id: Ref<number | null> = ref(null);
    const uploadAvatar = (value: FileList) => {
      urlFileLocal.value = URL.createObjectURL(value[0]);
      file.value = value[0];
      imageAPI.value = null;
    };

    watch(
      () => props.openUpdateHotProduct,
      (value) => {
        if (value) {
          id.value = props.dataUpdate?.id as number;
          name.value = props.dataUpdate?.name as string;
          code.value = props.dataUpdate?.code as string;
          embeddedUrl.value = props.dataUpdate?.embeddedUrl as string;
          status.value = props.dataUpdate?.status as number;
          urlFileLocal.value = props.dataUpdate?.imageUrl as string;
          if (props.dataUpdate?.file) {
            imageAPI.value = null;
          } else {
            imageAPI.value =
              // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
              configImg.API_IMAGE_ENDPOINT + props.dataUpdate?.imageUrl;
          }
        }
      }
    );
    const ResetData = () => {
      file.value = '';
      name.value = '';
      code.value = '';
      embeddedUrl.value = '';
      urlFileLocal.value = '';
      status.value = 2;
    };
    const SubbmitDataUpdate = () => {
      context.emit('click:CloseBtnUpdateHotProduct');
      context.emit('UpdateData', {
        file: file.value,
        name: name.value,
        code: code.value,
        status: status.value,
        embeddedUrl: embeddedUrl.value,
        imageUrl: urlFileLocal.value,
        id: id.value,
      });
      Notify.create({
        type: 'positive',
        message: i18n.global.t(
          'artist.dialogLabel.validateMessages.updateHotProductSccess'
        ),
        actions: [{ icon: 'close', color: 'white' }],
      });
    };
    const product_name = [
      (val?: string) =>
        (val && val.trim().length) ||
        i18n.global.t(
          'artist.artistInformation.validateMessages.requireProducName'
        ),
    ];
    const product_code = [
      (val?: string) =>
        (val && val.trim().length) ||
        i18n.global.t(
          'artist.artistInformation.validateMessages.requireProducCode'
        ),
    ];
    const url_embed = [
      (val?: string) =>
        (val && val.trim().length) ||
        i18n.global.t(
          'artist.artistInformation.validateMessages.requireUrlembed'
        ),
    ];
    const statusRules = [
      (val?: number) =>
        val !== undefined ||
        i18n.global.t(
          'artist.artistInformation.validateMessages.requireStatus'
        ),
    ];

    return {
      uploadAvatar,
      SubbmitDataUpdate,
      urlFileLocal,
      file,
      name,
      code,
      embeddedUrl,
      status,
      ResetData,
      product_name,
      product_code,
      url_embed,
      statusRules,
      statusOptions,
      id,
      configImg,
      imageAPI,
    };
  },

  emits: [
    'UpdateData',
    'update:statusHotProduct',
    'selectedFile',
    'click:CloseBtnUpdateHotProduct',
    'update:openUpdateHotProduct',
  ],
});
