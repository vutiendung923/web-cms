import { defineComponent, Ref, ref, watch } from 'vue';
import UploadImage from '../../../upload-image/index.vue';

export default defineComponent({
  components: {
    UploadImage,
  },
  props: {
    isOpenAddStory: {
      type: Boolean,
      required: true,
    },
  },

  setup(props, context) {
    const file: Ref<File | string> = ref('');
    const title: Ref<string> = ref('');
    const content: Ref<string> = ref('');
    const urlFileLocal: Ref<string> = ref('');
    const uploadStory = (value: FileList) => {
      urlFileLocal.value = URL.createObjectURL(value[0]);
      file.value = value[0];
    };
    watch(
      () => props.isOpenAddStory,
      (value) => {
        if (value) {
          ResetData();
        }
      }
    );
    const ResetData = () => {
      file.value = '';
      title.value = '';
      content.value = '';
      urlFileLocal.value = '';
    };
    const SubmitData = () => {
      context.emit('click:CloseBtnAddStory');
      context.emit('insertData', {
        file: file.value,
        title: title.value,
        content: content.value,
        imageUrl: urlFileLocal.value,
      });
    };

    return {
      uploadStory,
      SubmitData,
      urlFileLocal,
      file,
      title,
      content,
      ResetData,
    };
  },

  emits: [
    'insertData',
    'update:isOpenAddStory',
    'selectedFile',
    'click:CloseBtnAddStory',
  ],
});
