import { defineComponent, PropType, ref, watch } from 'vue';
import { i18n } from 'src/boot/i18n';
import { useRouter } from 'vue-router';
import { BannerType, StoriesType } from 'src/assets/type';
import UploadImage from 'components/upload-image/index.vue';
import { Dialog, Notify } from 'quasar';
import { config } from 'src/assets/configurations.example';

export default defineComponent({
  components: { UploadImage },

  props: {
    id: { number: String, required: true },
  
    banners: { type: Array as PropType<BannerType[]>, required: true },
    shortDescription: { type: Array, required: true },
    socialEmbedded: { type: Array, required: true },
    stories: { type: Array as PropType<StoriesType[]>, required: true },
    formatSchedules: { type: Array as PropType<string[]>, required: true },
  },

  setup(props, context) {
    const router = useRouter();
    const configImg = config;
    watch(
      () => props.banners.length,
      (value) => {
        slide.value = value - 1;
      }
    );
    watch(
      () => props.stories.length,
      (value) => {
        slideStory.value = value - 1;
      }
    );
    const slide = ref(0);
    const slideStory = ref(0);
    const editor = ref('Customize it.');

    const uploadBanner = (value: FileList) => {
      context.emit('selectedFile', encodeImageFileAsURL(value[0]));
      Notify.create({
        type: 'positive',
        message: i18n.global.t('artist.actionMessages.bannerAddAccess'),
        actions: [{ icon: 'close', color: 'white' }],
      });
    };
    const encodeImageFileAsURL = (file: File) => {
      return { url: URL.createObjectURL(file), file: file };
    };
    const deleteImage = (index: number) => {
      Dialog.create({
        title: i18n.global.t(
          'managingUnit.confirmActionsTitle.confirmDeleteManagingUnitsTitle'
        ),
        message: i18n.global.t(
          'artist.confirmActionsTitle.confirmDeleteBannerContent'
        ),
        cancel: i18n.global.t(
          'managingUnit.confirmActionsTitle.confirmDeleteManagingUnitsCancelBtnLabel'
        ),
        color: 'negative',
      }).onOk(() => {
        context.emit('deleteBanner', index);
        Notify.create({
          type: 'positive',
          message: i18n.global.t('artist.actionMessages.bannerDeleteAccess'),
          actions: [{ icon: 'close', color: 'white' }],
        });
      });
    };

    const deleteStory = (storyIdx: number) => {
      context.emit('confirmDeleteStory', storyIdx);
    };

    const uploadBannerImg = ref(null);
    const indexBanner = ref(0);
    const updateImage = (index: number) => {
      indexBanner.value = index;
      // eslint-disable-next-line
      // @ts-ignore
      // eslint-disable-next-line
      uploadBannerImg.value?.click();
    };

    const updateBanner = (value: FileList) => {
      context.emit('editBanner', {
        index: indexBanner.value,
        obj: encodeImageFileAsURL(value[0]),
      });

      Notify.create({
        type: 'positive',
        message: i18n.global.t('artist.actionMessages.bannerUpdateAccess'),
        actions: [{ icon: 'close', color: 'white' }],
      });
    };

    return {
      autoplay: ref(true),
      router,
      slide,
      slideStory,
      editor,
    
      uploadBanner,
      deleteImage,
      deleteStory,
      configImg,
      updateImage,
      uploadBannerImg,
      updateBanner,
    };
  },
  emits: [

    'update:shortDescription',
    'update:formatSchedules',
    'selectedFile',
    'deleteBanner',
    'openDialogUploadEmbed',
    'deleteImagesStories',
    'deleteStories',
    'confirmDeleteSocialEmbedded',
    'click:openAddStoryDialog',
    'click:openUpdateStoryDialog',
    'confirmDeleteStory',
    'editBanner',
  ],
});
