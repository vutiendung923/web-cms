import { defineComponent, PropType, ref, Ref } from 'vue';
import { i18n } from 'src/boot/i18n';
import UploadImage from '../../upload-image/index.vue';
import moment from 'moment';
import { FieldType, NationalityType } from 'src/assets/type';
// import { API_PATHS, config } from 'src/assets/configurations.example';
// import { api, BaseResponseBody } from 'src/boot/axios';
// import { AxiosResponse } from 'axios';
import {
  ProvinceType,
}  from 'src/assets/type'
export default defineComponent({

  props: {
    mnBookingPhone: { type: String, required: true },
    mnBookingEmail: { type: String, required: true },
    mnFbmess: { type: String, required: true },
    mnIns: { type: String, required: true },
    mnWhatsapp: { type: String, required: true },
    mnName: { type: String, required: true },
    mnPhone: { type: String, required: true },
    mnEmail: { type: String, required: true },
    hidden_img: { type: Boolean, required: true },
    id: { type: Number, required: true },
    artistCode: { type: String, required: true },
    avatar: { type: String, required: true },
    fullName: { type: String, required: true },
    artistName: { type: String, required: true },
    birthday: { type: String, required: true },
    sex: { type: Number, required: true },
    nationality: { type: Object as PropType<NationalityType>, required: true },
    status: { type: Number, required: true },
    address: { type: String ,required: true },
    fields: { type: Object as PropType<FieldType>, required: true },
    musics: {type: String, required: true },
    works: { type: Number, required: true },
    qualification: { type: Number, required: true },
    artistLevel: { type: Number, required: true },
    phoneNumber: { type: String, required: true },
    email: { type: String, required: true },
    phoneNumberAdminister: { type: String, required: true },
    emailAdminister: { type: String, required: true },
    facebook: { type: String, required: true },
    facebookMessage: { type: String, required: true },
    instagram: { type: String, required: true },
    whatsapp: { type: String, required: true },
    sexOptions: { type: Array, required: true },
    fieldOptions: { type: Array, required: true },
    nationalityOptions: { type: Array, required: true },

    professionOptions: { type: Array, required: true },
    artistLevelOptions: { type: Array, required: true },
    workOptions: { type: Array, required: true },
    musicOptions: { type:Array, required: true},
    favoriteScore: { type: Number, required: true },
    artistCodeRules: { type: Boolean, required: true },
    fullNameRules: { type: Boolean, required: true },
    // artistNameRules: { type: Boolean, required: true },
    // birthdayRules: { type: Boolean, required: true },
    // emailRules: { type: Boolean, required: true },
    emailAdministerRules: { type: Boolean, required: true },
    phoneNumberAdministerRules: { type: Boolean, required: true },
    // addressRules: { type: Boolean, required: true },
    account: { type: String, required: true },
    accountRules:{type:Boolean, required: true},
    // phoneNumberRules: { type: Boolean, required: true },
    // sexRules: { type: Boolean, required: true },
    // nationalityRules: { type: Boolean, required: true },
    // fieldRules: { type: Boolean, required: true },
    // musicsRules: { type: Boolean, required: true },
    // workRules: { type: Boolean, required: true },
    // qualificationRules: { type: Boolean, required: true },
    artistLevelRules: { type: Boolean, required: true },
    // errorMessPhoneNumber: { type: String, required: true },
    // errorMessEmail: { type: String, required: true },
    // errorMessmnBookingPhone: { type: String, required: true },
    // errorMessmnBookingEmail: { type: String, required: true },
    // mnBookingEmailRules: { type: Boolean, required: true },
    // mnBookingPhoneRules: { type: Boolean, required: true },
   
    options:  { type: String, required: true },
    provinceOptions: {type: Array, required: true},
    filterArrayOrganizational: {
      type: Array as PropType<ProvinceType[]>,
      required: true,
    }
  },
  
  
  components: {
    UploadImage,
  },
  
  

  setup(_, context) {
    const BirthdayDatePicker = ref(moment().format('YYYY/MM/DD'));
    const selectedFile = (value: FileList) => {
      if (value.length !== 0) {
        context.emit('SetAvatar', {
          file: value[0],
          url: URL.createObjectURL(value[0]),
        });
        context.emit('update:hidden_img', false);
      }
    };

    

 

    // const options = props.filterArrayOrganizational
  
    const filterFn = (val:string, update:(fn: () => void) => void) => {
     
      if (!val) {
        //  console.log(props.filterArrayOrganizational);
        update(() => {
          // options = 
        });
        return;
      }
      //  else {
      //   update(() => { 
      //     const needle = val.toLowerCase();
      //       options.value = filterArrayOrganizational.value.filter(
      //         (v: {  name: string }) =>
      //           v.name.toLowerCase().indexOf(needle) > -1
      //       );
      //   });
      // }
    }

    const upload = ref(null);
    const uploadBanner = () => {
      // eslint-disable-next-line
      // @ts-ignore
      // eslint-disable-next-line
      upload.value?.click();
    };
    const deleteAvatar = () => {
      context.emit('deleteAvatar');
    };
    const selectDatePicker = (value: string) => {
      context.emit('UpdateBirtday', formatDatePicker(value));
    };
    const formatDatePicker = (value: string) => {
      return moment(value).format('DD/MM/YYYY');
    };
    const openDialog: Ref<boolean> = ref(false);
    const onOKClick = () => {
      selectDatePicker(BirthdayDatePicker.value);
      openDialog.value = false;
    };
    // const onOKClick = () => {};
    const onCancelClick = () => {
      openDialog.value = false;
    };
      
 
   const errorMessAccount = i18n.global.t( 'artist.artistInformation.validateMessages.requireAccount'
   );
    const errorMessArtistCode = i18n.global.t(
      'artist.artistInformation.validateMessages.requireArtistCode'
    );
    const errorMessFullName = i18n.global.t(
      'artist.artistInformation.validateMessages.requireFullName'
    );
    const errorMessArtistName = i18n.global.t(
      'artist.artistInformation.validateMessages.requireArtistName'
    );
    const errorMessBirthday = i18n.global.t(
      'artist.artistInformation.validateMessages.requireBirthday'
    );
  
    const errorMessSex = i18n.global.t(
      'artist.artistInformation.validateMessages.requireSex'
    );
    const errorMessNationality = i18n.global.t(
      'artist.artistInformation.validateMessages.requireNationality'
    );
    const errorMessAddress = i18n.global.t(
      'artist.artistInformation.validateMessages.requireAddress'
    );
    const errorMessFields = i18n.global.t(
      'artist.artistInformation.validateMessages.requireField'
    );
    // const errorMessWorks = i18n.global.t(
    //   'artist.artistInformation.validateMessages.requiredWork'
    // );
    const errorMessQualification = i18n.global.t(
      'artist.artistInformation.validateMessages.requireQualification'
    );
    const errorMessArtistLevel = i18n.global.t(
      'artist.artistInformation.validateMessages.requireArtistLevel'
    );
    // onMounted(() => {
    // void  getProvinceOptions()
    // });
    return {
      check_infoBooking: ref(false),
      upload,
      uploadBanner,
      filterFn,
      selectedFile,
      deleteAvatar,
      BirthdayDatePicker,
      selectDatePicker,
      formatDatePicker,
      openDialog,
      onOKClick,
      onCancelClick,
      errorMessArtistCode,
      errorMessFullName,
      errorMessAccount,
      errorMessArtistName,
      errorMessBirthday,
      errorMessSex,
      errorMessNationality,
  
      errorMessAddress,
      errorMessFields,
      // errorMessWorks,
      errorMessQualification,
      errorMessArtistLevel,
      // filterArrayOrganizational,
      // getProvinceOptions
    };
  },

  emits: [
    'update:emailAdminister',
    'update:phoneNumberAdminister',
    'update:artistCode',
    'update:fullName',
    'update:avatar',
    'update:artistName',
    'update:birthday',
    'update:email',
    'update:facebook',
    'update:facebookMessage',
    'update:instagram',
    'update:whatsapp',
    'update:address',
    'update:phoneNumber',
    'update:sex',
    'update:nationality',
    'update:fields',
    'update:works',
    'update:favoriteScore',
    'update:check_infoBooking',
    'update:qualification',
    'update:artistLevel',
    'update:hidden_img',
    'update:status',
    'update:mnName',
    'update:mnPhone',
    'update:mnEmail',
    'update:mnBookingPhone',
    'update:mnBookingEmail',
    'update:mnFbmess',
    'update:mnIns',
    'update:mnWhatsapp',
    'update:musics',
    'update:account',
    'addNewArtist',
    'SetAvatar',
    'deleteAvatar',
    'UpdateBirtday',
   
  ],
   
});


  


