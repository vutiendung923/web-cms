import { defineComponent, PropType, Ref, ref, watch } from 'vue';
import { i18n } from 'src/boot/i18n';

import { BankAccountType, CardBankType, TypeCardType } from 'src/assets/type';
import { Notify } from 'quasar';

export default defineComponent({
  props: {
    isOpenEditAccountBankDialog: {
      type: Boolean,
      required: true,
    },
    bankAccounts: {
      type: Array as PropType<BankAccountType[]>,
      required: true,
    },
    cardBankOptions: {
      type: Array as PropType<CardBankType[]>,
      required: true,
    },
    typeBankOptions: {
      type: Array as PropType<TypeCardType[]>,
      required: true,
    },
    rowBankAccIdx: {
      type: Number,
      required: true,
    },
    rowDataAccBank: {
      type: Object as PropType<BankAccountType>,
      required: true,
    },
  },

  setup(props, context) {
    watch(
      () => props.isOpenEditAccountBankDialog,
      (value) => {
        if (value) {
          accountOwner.value = props.rowDataAccBank.accountOwner;
          accountNumber.value = props.rowDataAccBank.accountNumber;
          cardNumber.value = props.rowDataAccBank.cardNumber;
          bank.value = props.rowDataAccBank.bank.id;
          cardType.value = props.rowDataAccBank.cardType.id;
          isDefault.value = props.rowDataAccBank.isDefault;
        }
      }
    );
    const accountOwnerRules = [
      (val?: string) =>
        (val && val.trim().length) ||
        i18n.global.t(
          'artist.dialogLabel.validateMessages.requireAccountOwner'
        ),
    ];
    const accountNumberRules = [
      (val?: string) =>
        (val && val.trim().length) ||
        i18n.global.t(
          'artist.dialogLabel.validateMessages.requireAccountNumber'
        ),
    ];
    const cardNumberRules = [
      (val?: string) =>
        (val && val.trim().length) ||
        i18n.global.t('artist.dialogLabel.validateMessages.requireCardNumber'),
    ];
    const bankNameRules = [
      (val: number | null) =>
        val !== null ||
        i18n.global.t('artist.dialogLabel.validateMessages.requireBankName'),
    ];
    const cardTypeRules = [
      (val: number | null) =>
        val !== null ||
        i18n.global.t('artist.dialogLabel.validateMessages.requireCardType'),
    ];
    const confirmEditAccBank = () => {
      let bankFilter = {};
      props.cardBankOptions.map((item) => {
        if (item.id === bank.value) {
          bankFilter = item;
        }
      });

      let typeCardFilter = {};
      props.typeBankOptions.map((item) => {
        if (item.id === cardType.value) {
          typeCardFilter = item;
        }
      });
      let hasError = false;
      for (let index = 0; index < props.bankAccounts.length; index++) {
        const element = props.bankAccounts[index];
        if (
          element.accountNumber == accountNumber.value &&
          index !== props.rowBankAccIdx
        ) {
          hasError = true;
          Notify.create({
            type: 'negative',
            message: i18n.global.t(
              'artist.dialogLabel.validateMessages.accountNumberExits'
            ),
            actions: [{ icon: 'close', color: 'white' }],
          });
        }
      }
      if (!hasError) {
        context.emit('editBankAccount', {
          accountOwner: accountOwner.value,
          accountNumber: accountNumber.value,
          cardNumber: cardNumber.value,
          bank: bankFilter,
          cardType: typeCardFilter,
          isDefault: isDefault.value,
        });
        Notify.create({
          type: 'positive',
          message: i18n.global.t(
            'artist.dialogLabel.validateMessages.editAccess'
          ),
          actions: [{ icon: 'close', color: 'white' }],
        });
      }
    };
    const accountOwner: Ref<string | null> = ref(null);
    const accountNumber: Ref<string> = ref('');
    const cardNumber: Ref<string> = ref('');
    const bank: Ref<number | null> = ref(null);
    const cardType: Ref<number | null> = ref(null);
    const isDefault: Ref<number> = ref(2);

    return {
      accountOwnerRules,
      bankNameRules,
      cardTypeRules,
      accountNumberRules,
      cardNumberRules,
      accountOwner,
      accountNumber,
      cardNumber,
      bank,
      cardType,
      isDefault,
      confirmEditAccBank,
    };
  },
  emits: [
    'update:isOpenEditAccountBankDialog',
    'click:CloseBtn',
    'editBankAccount',
    'update:accountNumber',
    'update:cardNumber',
    'update:bankName',
    'update:cardType',
    'update:isDefault',
  ],
});
