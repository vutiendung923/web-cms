import { i18n } from 'src/boot/i18n';
import { defineComponent, onMounted, PropType, Ref, ref } from 'vue';
import AddNewBankAccountDialog from 'components/artist-information/bank-account/add-new-bank-account-dialog/index.vue';
import { BankAccountType } from 'src/assets/type';
export default defineComponent({
  components: {
    AddNewBankAccountDialog,
  },
  props: {
    bankAccounts: {
      type: Array as PropType<BankAccountType[]>,
      required: true,
    },
  },
  setup(props, context) {
    const userTableColumnsBankAccount = [
      {
        name: 'accountOwner',
        field: 'accountOwner',
        required: true,
        label: i18n.global.t(
          'artist.bankAccount.tableColumnsBank.accountOwner'
        ),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'accountNumber',
        field: 'accountNumber',
        required: true,
        label: i18n.global.t('artist.bankAccount.tableColumnsBank.idCard'),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'cardNumber',
        field: 'cardNumber',
        required: true,
        label: i18n.global.t('artist.bankAccount.tableColumnsBank.numberCard'),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'bank',
        field: 'bank',
        required: true,
        label: i18n.global.t('artist.bankAccount.tableColumnsBank.bankName'),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'cardType',
        field: 'cardType',
        required: true,
        label: i18n.global.t('artist.bankAccount.tableColumnsBank.cardType'),
        headerStyle: 'text-align: center !important;',
        align: 'left',
        sortable: false,
      },
      {
        name: 'isDefault',
        field: 'isDefault',
        required: true,
        label: i18n.global.t('artist.bankAccount.tableColumnsBank.default'),
        align: 'center',
        sortable: false,
      },
      {
        name: 'action',
        field: 'action',
        required: true,
        label: i18n.global.t('artist.bankAccount.tableColumnsBank.action'),
        align: 'center',
        sortable: false,
      },
    ];
    const userTableRowsBankAccount: Ref<BankAccountType[]> = ref([]);
    const isOpenDialog = ref(false);
    const dataBankAccount: Ref<BankAccountType[]> = ref([]);

    const changeIsDefault = (isDefault: number, idxAcc: number) => {
      context.emit('confirmChangeIsDefault', {
        isDefault: isDefault,
        idxAcc: idxAcc,
      });
    };
    onMounted(() => {
      dataBankAccount.value = props.bankAccounts;
    });
    return {
      isOpenDialog,
      userTableColumnsBankAccount,
      userTableRowsBankAccount,
      dataBankAccount,
      changeIsDefault,
    };
  },
  emits: [
    'click:addBankBtn',
    'confirmDeleteAccBank',
    'clickEditAccBankBtn',
    'confirmChangeIsDefault',
  ],
});
