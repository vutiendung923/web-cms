import { defineComponent } from 'vue';
import { EditMode } from 'src/assets/enums';
import { i18n } from 'src/boot/i18n';
import { useStore } from '../../store/index';
import { Page } from 'src/store/authentication/state';

const groupNameRules = [
  (val?: string) =>
    (val && val.trim().length) ||
    i18n.global.t('userGroupPage.groupInfo.validateMessages.requireGroupName'),
];

const groupDescriptionRules = [
  (val?: string) =>
    (val && val.trim().length) ||
    i18n.global.t(
      'userGroupPage.groupInfo.validateMessages.requireGroupDescription'
    ),
];

export const GroupInfoScript = defineComponent({
  props: {
    isDisable: {
      type: Number,
      required: true,
    },
    groupName: {
      type: String,
      required: true,
    },
    groupDescription: {
      type: String,
      required: true,
    },
    selectedPageRoles: {
      type: Array,
      required: true,
    },
  },
  emits: [
    'resetGroupInfo',
    'update:groupName',
    'update:groupDescription',
    'saveGroupInfo',
    'deleteGroup',
    'update:selectedPageRoles',
  ],
  setup() {
    const $store = useStore();

    const pagesRoles = [...$store.state.authentication.pageList].reduce(
      (acc: Page[], page) => {
        const newPage = { ...page };
        newPage.name = newPage.pageName;
        newPage.formatted_role_list = newPage.formatted_role_list?.map(
          (role) => {
            const newRole = { ...role };
            newRole.id = `${newRole.id || ''}-${newPage.id}`;
            newRole.name = newRole.roleName;
            return newRole;
          }
        );

        acc.push(newPage);
        return acc;
      },
      []
    );
    return { EditMode, groupNameRules, groupDescriptionRules, pagesRoles };
  },
});
